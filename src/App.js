import { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

import { registerScreens } from './screens';
import configureStore from './store/configureStore';
// import appStyle from './resources/values/appStyle';

const store = configureStore();
registerScreens(store, Provider);

// const App = () => {
//   return (
//     Navigation.startSingleScreenApp({
//       screen: {
//         screen: 'yourbandung.Maps',
//         title: 'YourBandung X YourWeekdays'
//       }
//     })
//   );
// };

class App extends Component {

  constructor(props) {
    super(props);
    this.startApp();
	}    

  startApp() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'yourbandung.Maps',
        title: 'YourBandung X YourWeekdays'
      }
    });
  }
}

export default App;
