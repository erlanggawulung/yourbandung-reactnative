const toIDR = (money) => {
  const out = money.toString().split('').reverse().join('')
    .match(/.{1,3}/g)
    .join('.')
    .split('')
    .reverse()
    .join('');
  return `Rp ${out}`;
};

export { toIDR };
