import React, { Component } from 'react';
import { View, ScrollView, Text, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';

//
import {
  Card,
  CardSection,
  Button,
  get619RatioHeight
} from '../../shared_components';
import TabBar from '../_global/TabBar';
import { getProfile, logoutUser } from '../../actions/AuthActions';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth) / 2;

class Me extends Component {
  componentWillMount() {
    this.props.getProfile(this.props.navigator);
  }

  onLogout() {
    this.props.logoutUser(this.props.navigator);
    // debug
    // console.log('logout');
  }

  render() {
    const { containerStyle } = styles;
    if (this.props.isLoggedIn) {
      return (
        <View style={containerStyle}>
          <ScrollView>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  Profile
                </Text>
              </CardSection>     
              <CardSection style={{ height: contentHeight + 10 }}> 
                <View>
                  <Image 
                    style={{
                      height: contentHeight,
                      width: wWidth / 4,
                    }}
                    source={{ uri: this.props.profile.path_photo }}
                  />
                </View>
                <View style={{ paddingLeft: 10 }}>
                  <Text>
                    {this.props.profile.name}
                  </Text>
                  <Text>
                    {this.props.profile.email}
                  </Text>
                </View>
              </CardSection> 
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  My Voucher
                </Text>
              </CardSection>                 
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  Web Login
                </Text>
              </CardSection>      
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  How To Redeem
                </Text>
              </CardSection>      
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  Term's and Conditions
                </Text>
              </CardSection>      
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  FAQ's
                </Text>
              </CardSection>      
            </Card>
            <Card>
              <CardSection>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  About Us
                </Text>
              </CardSection>      
            </Card>
            <Card style={{ marginBottom: 10, height: 50 }}>
              {/* <CardSection style={{ height: 50 }}> */}
                <Button
                  style={{ backgroundColor: '#FFA619', borderColor: '#FFA619' }} 
                  onPress={this.onLogout.bind(this)}
                >
                  LOG OUT
                </Button>
              {/* </CardSection> */}
            </Card>
          </ScrollView>
          <TabBar
            navigator={this.props.navigator}
            app={this.props.app}
          />
        </View>
      );
    }
    return (
      <View />
    );    
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  textStyle: {
    fontSize: 20
  },
  cardStyle: {
    // height: wHeight - tabBarHeight
  }
};

const mapStateToProps = state => {
  const { app } = state;
  const { profile, isLoggedIn } = state.auth;

  // debug
  console.log(profile);
  
  return { app, profile, isLoggedIn };
};

export default connect(mapStateToProps, { getProfile, logoutUser })(Me);
