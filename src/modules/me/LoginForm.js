import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

//
import {
  Card,
  CardSection,
  Button,
  InputWithoutLabel
} from '../../shared_components';
import TabBar from '../_global/TabBar';
import Images from '../../resources/images';
import { emailChanged, passwordChanged, loginUser } from '../../actions/AuthActions';

class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onSignupClicked() {
    this.props.navigator.push({
      screen: 'yourbandung.Signup',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  onLogin() {
    this.props.loginUser(this.props);
  }

  renderButton() {
    return (
      <Button 
        style={{ backgroundColor: '#FFA619', borderColor: '#FFA619' }} 
        onPress={this.onLogin.bind(this)}
      >
        LOG IN
      </Button>
    );
  }

  renderMessage() {
    if (this.props.error !== '') {
      return (
        <CardSection style={{ alignItems: 'center' }}>
          <Text>
            {this.props.error}
          </Text>              
        </CardSection>
      );
    }
  }

  render() {
    return (
      <View 
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
          }}
        >
          <Card style={{ borderWidth: 0 }}>
            <CardSection 
              style={{ 
                  justifyContent: 'center', 
                  alignItems: 'center', 
                  height: 70  
                }}
            >
              <Image
                style={{ height: 50, width: 150 }} 
                source={Images.dealsLogo} 
              />
            </CardSection>
          </Card>
          <Card>
            <CardSection style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                LOG IN
              </Text>
            </CardSection>          
            <CardSection>
              <InputWithoutLabel 
                placeholder="user@gmail.com"
                onChangeText={this.onEmailChange.bind(this)}
                value={this.props.email}
              />
            </CardSection>
            <CardSection>
              <InputWithoutLabel 
                placeholder="password"
                onChangeText={this.onPasswordChange.bind(this)}
                value={this.props.password}
                secureTextEntry
              />            
            </CardSection>
            {this.renderMessage()}                                      
            <CardSection style={{ flexDirection: 'column', height: 120 }}>
              {this.renderButton()}
              <View 
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 10
                }}
              >
                <TouchableOpacity>
                  <Text>
                    Forgot Password
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.onSignupClicked.bind(this)}>
                  <Text>
                    New Here? Sign Up
                  </Text>
                </TouchableOpacity>
              </View>
            </CardSection>
          </Card>
        </View>      
          <View>
            <TabBar
              navigator={this.props.navigator}
              app={this.props.app}
            />
          </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { email, password, error } = auth;

  // debug
  // console.log(email, password);

  return { email, password, error };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
