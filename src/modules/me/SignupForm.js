import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { connect } from 'react-redux';
//
import {
  Card,
  CardSection,
  Button,
  InputWithoutLabel,
  Spinner
} from '../../shared_components';
import TabBar from '../_global/TabBar';
// import Images from '../../resources/images';
import { 
  nameChanged,
  emailChanged, 
  genderChanged,
  passwordChanged, 
  passwordConfirmationChanged,
  signUpUser
} from '../../actions/AuthActions';

class SignupForm extends Component {
  componentDidMount() {
    // set default gender
    this.props.genderChanged('Male');
  }

  onNameChange(text) {
    this.props.nameChanged(text);
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onPasswordConfirmationChange(text) {
    this.props.passwordConfirmationChanged(text);
  }

  onLoginClicked() {
    this.props.navigator.resetTo({
      screen: 'yourbandung.Login',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  onGenderChange(index, value) {
    this.props.genderChanged(value);
    // debug
    // console.log(index, value);
  }

  onSignUp() {
    this.props.signUpUser(this.props);
  }

  renderMessage() {
    if (this.props.error !== '') {
      return (
        <CardSection style={{ alignItems: 'center' }}>
          <Text>
            {this.props.error}
          </Text>              
        </CardSection>
      );
    }

    if (this.props.successMessage !== '') {
      return (
        <CardSection style={{ alignItems: 'center' }}>
          <Text>
            {this.props.successMessage}
          </Text>              
        </CardSection>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size='large' />;
    } 

    return (
      <Button
        style={{ backgroundColor: '#FFA619', borderColor: '#FFA619' }} 
        onPress={this.onSignUp.bind(this)}
      >
        SIGN UP
      </Button>
    );
  }  

  render() {    
    return (
      <View 
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <View>
          <Card>
            <CardSection style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                SIGN UP
              </Text>
            </CardSection>          
            <CardSection>
              <InputWithoutLabel 
                placeholder="John Doe"
                onChangeText={this.onNameChange.bind(this)}
                value={this.props.name}
              />
            </CardSection>
            <CardSection>
              <InputWithoutLabel 
                placeholder="john@gmail.com"
                onChangeText={this.onEmailChange.bind(this)}
                value={this.props.email}
              />
            </CardSection>
            <CardSection>
              <RadioGroup
                size={24}
                thickness={2}
                color='#989696'
                selectedIndex={0}
                onSelect={(index, value) => this.onGenderChange(index, value)}
                style={{ flexDirection: 'row' }}
              >
                <RadioButton 
                  value={'Male'}
                >
                  <Text>Male</Text>
                </RadioButton>
                <RadioButton 
                  value={'Female'}
                >
                  <Text>Female</Text>
                </RadioButton>
              </RadioGroup>
            </CardSection>
            <CardSection>
              <InputWithoutLabel 
                placeholder="password"
                onChangeText={this.onPasswordChange.bind(this)}
                value={this.props.password}
                secureTextEntry
              />            
            </CardSection>
            <CardSection>
              <InputWithoutLabel 
                placeholder="re-type password"
                onChangeText={this.onPasswordConfirmationChange.bind(this)}
                value={this.props.passwordConfirmation}
                secureTextEntry
              />            
            </CardSection>
            {this.renderMessage()}              
            <CardSection style={{ flexDirection: 'column', height: 60 }}>
              {this.renderButton()}              
            </CardSection>
          </Card>
        </View>
        <View>
          <TabBar
            navigator={this.props.navigator}
            app={this.props.app}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth, maps }) => {
  const { name, 
    email, 
    gender, 
    password, 
    passwordConfirmation, 
    loading, 
    successMessage, 
    error } = auth;

    const {
      coordinate
    } = maps.userPosition;

  // debug
  // console.log(maps);

  return { name, 
    email, 
    gender, 
    password, 
    passwordConfirmation, 
    loading, 
    successMessage, 
    error, 
    coordinate };
};

export default connect(mapStateToProps, {
  nameChanged,
  emailChanged,
  genderChanged,
  passwordChanged,
  passwordConfirmationChanged,
  signUpUser
})(SignupForm);
