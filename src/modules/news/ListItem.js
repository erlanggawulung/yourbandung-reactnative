import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';

//
import { CardSection } from '../../shared_components';
import { newsSelected } from '../../actions';

class ListItem extends Component {
  onRowPress() {
    this.props.newsSelected(this.props.news);
    
    const { title } = this.props.news;    
    this.props.navigator.resetTo({
      screen: 'yourbandung.NewsContent',
      title       
    });

    //debug
    // console.log('pressed!');
    // console.log(this.props);
  }

  render() {    
    const { title, published } = this.props.news;
    const { titleStyle, publishedStyle } = styles;
    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>
          <CardSection style={{ height: 75, justifyContent: 'center', flexDirection: 'column' }}>
            <Text numberOfLines={2} style={titleStyle}>
              { title }
            </Text>
            <Text style={publishedStyle}>
              { published.substring(0, 22) }
            </Text>
          </CardSection>   
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 17,
    paddingLeft: 15
  },
  publishedStyle: {
    fontSize: 10,
    paddingLeft: 15
  }
};

const mapStateToProps = state => {
  const { app } = state;
  const { selectedNews } = state.news;

  return { app, selectedNews };
};

export default connect(mapStateToProps, { newsSelected })(ListItem);
