import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

//
import TabBar from '../_global/TabBar';
import SubNewsList from './SubNewsList';
import { newsFetch } from '../../actions';

class News extends Component {
  componentWillMount() {
    this.props.newsFetch();    
  }
  
  render() {
    // debug
    // console.log(this.props);
    const { containerStyle } = styles;

    return (
      <View style={containerStyle}>
        <ScrollableTabView
          initialPage={0}
          tabBarActiveTextColor='#F27E0B'
          tabBarUnderlineStyle={{ backgroundColor: '#F27E0B' }}
          renderTabBar={() => <ScrollableTabBar activeTab={0} />}
        >
          <SubNewsList 
            tabLabel="Semua" 
            navigator={this.props.navigator}
            newsList={this.props.semua}
            loading={this.props.loading}
          />
          <SubNewsList 
            tabLabel="Berita" 
            navigator={this.props.navigator}
            newsList={this.props.berita}
            loading={this.props.loading}
          />
          <SubNewsList 
            tabLabel="Acara" 
            navigator={this.props.navigator}
            newsList={this.props.acara}
            loading={this.props.loading}
          />
          <SubNewsList 
            tabLabel="Kuliner" 
            navigator={this.props.navigator}
            newsList={this.props.kuliner}
            loading={this.props.loading}
          />
          <SubNewsList 
            tabLabel="Infografis" 
            navigator={this.props.navigator}
            newsList={this.props.infografis}
            loading={this.props.loading}
          />
          <SubNewsList 
            tabLabel="Komik" 
            navigator={this.props.navigator}
            newsList={this.props.komik}
            loading={this.props.loading}
          />
        </ScrollableTabView>
        <TabBar
          navigator={this.props.navigator}
          app={this.props.app}
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#f8f8f8'
  },
  textStyle: {
    fontSize: 20
  }
};

const mapStateToProps = state => {
  const { app } = state;
  const { loading } = state.news;
  const { semua, berita, kuliner, acara, infografis, komik } = state.news.newsList;

  return { app, loading, semua, berita, kuliner, acara, infografis, komik };
};

export default connect(mapStateToProps, { newsFetch })(News);
