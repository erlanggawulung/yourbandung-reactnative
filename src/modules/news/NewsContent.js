import React, { Component } from 'react';
import { connect } from 'react-redux';
import { WebView } from 'react-native';


//
import Images from '../../resources/images';
  
class NewsContent extends Component {
  static navigatorButtons = {
    rightButtons: [],
    leftButtons: [
      {
        icon: Images.leftArrow, // for icon button, provide the local image asset name
        id: 'back_to_news' // id for this button, given in onNavigatorEvent(event) to help
      }      
    ]
  };

  constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
    if (event.type === 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id === 'back_to_news') {
        // debug
        // console.log('left add pressed');
        this.props.navigator.resetTo({
          screen: 'yourbandung.News',
          title: 'YourBandung X YourWeekdays'      
        });
      }
    }
  }

  render() {
    // debug
    // console.log(this.props);
    
    const { links } = this.props.selectedNews;
    const link = links[0].url;

    return (
      <WebView
        source={{ uri: link }}
      />
    );
  }
}

const mapStateToProps = state => {
  const { app } = state;
  const { selectedNews } = state.news;

  return { app, selectedNews };
};

export default connect(mapStateToProps, {})(NewsContent);
