import React, { Component } from 'react';
import { View, Text } from 'react-native';

//
import TabBar from '../_global/TabBar';

class Home extends Component {
  render() {
    const { containerStyle, textStyle, cardStyle } = styles;

    return (
      <View style={containerStyle}>
        <View style={cardStyle}>
          <Text style={textStyle}>
            Home
          </Text>
        </View>
        <TabBar />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#f8f8f8'
  },
  textStyle: {
    fontSize: 20
  },
  cardStyle: {
    // height: wHeight - tabBarHeight
  }
};

export default Home;
