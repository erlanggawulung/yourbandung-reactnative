import React, { Component } from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  Image
} from 'react-native';
import { connect } from 'react-redux';

//
import { tabChanged } from '../../actions';
import {
  ON_MAPS,
  ON_NEWS,
  ON_VOUCHER,
  ON_PARTNER,
  ON_ME
} from '../../resources/values/tabType';
import Icons from '../../resources/icons';

const wWidth = Dimensions.get('window').width;

const mapsUnselected = Icons.mapsUnselected;
const mapsSelected = Icons.mapsSelected;
const newsUnselected = Icons.newsUnselected;
const newsSelected = Icons.newsSelected;
const voucherUnselected = Icons.voucherUnselected;
const voucherSelected = Icons.voucherSelected;
const partnerUnselected = Icons.partnerUnselected;
const partnerSelected = Icons.partnerSelected;
const meUnselected = Icons.meUnselected;
const meSelected = Icons.meSelected;

const styles = {
  containerStyle: {
    width: wWidth,
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  buttonStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingVertical: 4
  },
  textStyle: {
    fontSize: 12,
    textAlign: 'center',
    color: '#989696'
  },
  textSelectedStyle: {
    fontSize: 12,
    textAlign: 'center',
    color: '#F27E0B'
  },
  touchableStyle: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    width: 30,
    height: 30
  }
};

const { 
  containerStyle, 
  buttonStyle, 
  touchableStyle, 
  textStyle, 
  imageStyle, 
  textSelectedStyle 
} = styles;

class TabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: this.props.app.tab
    };
  }

  onMapsClicked() {
    this.props.tabChanged(ON_MAPS);
    this.props.navigator.resetTo({
      screen: 'yourbandung.Maps',
      title: 'YourBandung X YourWeekdays'      
    });

    // debug
    // console.log(this.props);
  }

  onNewsClicked() {
    this.props.tabChanged(ON_NEWS);
    this.props.navigator.resetTo({
      screen: 'yourbandung.News',
      title: 'YourBandung X YourWeekdays'      
    });

    // debug
    // console.log(this.props);
  }

  onVoucherClicked() {
    this.props.tabChanged(ON_VOUCHER);
    this.props.navigator.resetTo({
      screen: 'yourbandung.Voucher',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  onPartnerClicked() {
    this.props.tabChanged(ON_PARTNER);
    this.props.navigator.resetTo({
      screen: 'yourbandung.Partner',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  onMeClicked() {
    this.props.tabChanged(ON_ME);
    this.props.navigator.resetTo({
      screen: 'yourbandung.Me',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  renderMapsButton() {
    if (this.state.tab === ON_MAPS) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={mapsSelected}
            />
            <Text style={textSelectedStyle}>
              Maps
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          style={touchableStyle}
          onPress={this.onMapsClicked.bind(this)}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={mapsUnselected}
          />
          <Text style={textStyle}>
            Maps
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderNewsButton() {
    if (this.state.tab === ON_NEWS) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={newsSelected}
            />
            <Text style={textSelectedStyle}>
              News
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          style={touchableStyle}
          onPress={this.onNewsClicked.bind(this)}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={newsUnselected}
          />
          <Text style={textStyle}>
            News
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderVoucherButton() {
    if (this.state.tab === ON_VOUCHER) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={voucherSelected}
            />
            <Text style={textSelectedStyle}>
              Voucher
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          style={touchableStyle}
          onPress={this.onVoucherClicked.bind(this)}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={voucherUnselected}
          />
          <Text style={textStyle}>
            Voucher
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderPartnerButton() {
    if (this.state.tab === ON_PARTNER) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={partnerSelected}
            />
            <Text style={textSelectedStyle}>
              Partner
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          style={touchableStyle}
          onPress={this.onPartnerClicked.bind(this)}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={partnerUnselected}
          />
          <Text style={textStyle}>
            Partner
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderMeButton() {
    if (this.state.tab === ON_ME) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={meSelected}
            />
            <Text style={textSelectedStyle}>
              Me
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          style={touchableStyle}
          onPress={this.onMeClicked.bind(this)}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={meUnselected}
          />
          <Text style={textStyle}>
            Me
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    // console.log(this.props);
    
    return (
      <View style={containerStyle}>
        {this.renderMapsButton()}
        {this.renderNewsButton()}
        {this.renderVoucherButton()}
        {this.renderPartnerButton()}
        {this.renderMeButton()}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { app } = state;
  return { app };
};

export default connect(mapStateToProps, { tabChanged })(TabBar);
