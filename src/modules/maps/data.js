//
import Icons from '../../resources/icons';
import Images from '../../resources/images';

export const markers = [
  {
    key: '1',
    title: 'Museum Pos Indonesia',
    category: 'tour',
    imageSelected: Icons.tourSelected,
    imageUnselected: Icons.tourUnselected,
    imageDetail: Images.restaurant,
    address: 'Jl. Cilaki No.73, Citarum, Bandung Wetan, Kota Bandung, Jawa Barat 40115',
    openHours: '8:00AM-4:30PM',
    coordinate: {
      latitude: -6.902305,
      longitude: 107.619966,
    },
  },
  {
    key: '2',
    title: 'Vio Hotel Cimanuk',
    category: 'hotel',
    imageSelected: Icons.hotelSelected,
    imageUnselected: Icons.hotelUnselected,
    imageDetail: Images.restaurant,
    address: 'Jl. Cimanuk No.15, Citarum, Bandung Wetan, Kota Bandung, Jawa Barat 40115',
    openHours: '24 Jam',
    coordinate: {
      latitude: -6.903123,
      longitude: 107.621288,
    },
  },
  {
    key: '3',
    title: 'Bus Terminal Dipatiukur',
    category: 'transportation',
    imageSelected: Icons.transportationSelected,
    imageUnselected: Icons.transportationUnselected,
    imageDetail: Images.restaurant,
    address: 'Terminal Dipatiukur, JL. Dipatiukur, Lebak Siliwangi, 40132 Bandung, Indonesia, Lebak Gede, Coblong, Bandung City, West Java',
    openHours: '24 Jam',
    coordinate: {
      latitude: -6.892881,
      longitude: 107.618095,
    },
  },
  {
    key: '4',
    title: 'Restaurant Sindang Reret',
    category: 'culinary',
    imageSelected: Icons.culinarySelected,
    imageUnselected: Icons.culinaryUnselected,
    imageDetail: Images.restaurant,
    address: 'Jl. Surapati No.53, Sadang Serang, Coblong, Kota Bandung, Jawa Barat 40133',
    openHours: '24 Jam',
    coordinate: {
      latitude: -6.899095,
      longitude: 107.619788,
    },
  },
  {
    key: '5',
    title: 'Taman Lansia',
    category: 'other',
    imageSelected: Icons.otherSelected,
    imageUnselected: Icons.otherUnselected,
    imageDetail: Images.restaurant,
    address: 'Jl. Cisangkuy, Citarum, Bandung Wetan, Kota Bandung, Jawa Barat 40123',
    openHours: '24 Jam',
    coordinate: {
      latitude: -6.901705,
      longitude: 107.620464,
    },
  },
];
