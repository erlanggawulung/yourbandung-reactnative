import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

//
import { 
  openCloseFilter,
  setFilter
 } from '../../actions';
import {
  TOUR, 
  HOTEL,
  TRANSPORTATION,
  CULINARY,  
  OTHER
} from '../../resources/values/markerType';
import Icons from '../../resources/icons';

const styles = {
  viewStyle: {
    backgroundColor: '#F27E0B',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    height: 40,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  filterStyle: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    height: 70,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF'
  },
  buttonStyle: {
    // flex: 1,
    // backgroundColor: '#ffffff',
    // paddingVertical: 4
  },
  touchableStyle: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    width: 30,
    height: 30
  }
};

const { imageStyle, touchableStyle, buttonStyle } = styles;

class MapsHeader extends Component {
  onButtonPress() {
    this.props.openCloseFilter();

    // debug
    // console.log('button pressed!');
  }

  onTourClicked() {
    const { categoryStatusList } = this.props.maps;
    this.props.setFilter(categoryStatusList, TOUR);

    // debug
    // console.log(categoryStatusList);
    // console.log(TOUR);
  }

  onHotelClicked() {
    const { categoryStatusList } = this.props.maps;
    this.props.setFilter(categoryStatusList, HOTEL);
  }

  onTransportationClicked() {
    const { categoryStatusList } = this.props.maps;
    this.props.setFilter(categoryStatusList, TRANSPORTATION);
  }

  onCulinaryClicked() {
    const { categoryStatusList } = this.props.maps;
    this.props.setFilter(categoryStatusList, CULINARY);
  }

  onOtherClicked() {
    const { categoryStatusList } = this.props.maps;
    this.props.setFilter(categoryStatusList, OTHER);
  }

  renderTour() {
    const { tour } = this.props.maps.categoryStatusList;
    
    if (tour) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            onPress={this.onTourClicked.bind(this)}
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={Icons.filterTourSelected}
            />
            <Text style={{ color: '#CC0561' }}>Wisata</Text>          
          </TouchableOpacity>
        </View>
      );  
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          onPress={this.onTourClicked.bind(this)}
          style={touchableStyle}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={Icons.filterTourUnselected}
          />
          <Text style={{ color: '#989696' }}>Wisata</Text>          
        </TouchableOpacity>
      </View>
    );
  }  

  renderHotel() {
    const { hotel } = this.props.maps.categoryStatusList;
    
    if (hotel) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            onPress={this.onHotelClicked.bind(this)}
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={Icons.filterHotelSelected}
            />
            <Text style={{ color: '#FFA619' }}>Hotel</Text>          
          </TouchableOpacity>
        </View>
      );  
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          onPress={this.onHotelClicked.bind(this)}
          style={touchableStyle}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={Icons.filterHotelUnselected}
          />
          <Text style={{ color: '#989696' }}>Hotel</Text>          
        </TouchableOpacity>
      </View>          
    );
  }

  renderTransportation() {
    const { transportation } = this.props.maps.categoryStatusList;
    
    if (transportation) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            onPress={this.onTransportationClicked.bind(this)}
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={Icons.filterTransportationSelected}
            />
            <Text style={{ color: '#DD0404' }}>Transportasi</Text>          
          </TouchableOpacity>
        </View>
      );  
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          onPress={this.onTransportationClicked.bind(this)}
          style={touchableStyle}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={Icons.filterTransporationUnselected}
          />
          <Text style={{ color: '#989696' }}>Transportasi</Text>          
        </TouchableOpacity>
      </View>         
    );
  }

  renderCulinary() {
    const { culinary } = this.props.maps.categoryStatusList;
    
    if (culinary) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            onPress={this.onCulinaryClicked.bind(this)}
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={Icons.filterCulinarySelected}
            />
            <Text style={{ color: '#05CC2C' }}>Kuliner</Text>          
          </TouchableOpacity>
        </View>
      );  
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          onPress={this.onCulinaryClicked.bind(this)}
          style={touchableStyle}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={Icons.filterCulinaryUnselected}
          />
          <Text style={{ color: '#989696' }}>Kuliner</Text>          
        </TouchableOpacity>
      </View>          
    );
  }
  
  renderOther() {
    const { other } = this.props.maps.categoryStatusList;
    
    if (other) {
      return (
        <View style={buttonStyle}>
          <TouchableOpacity 
            onPress={this.onOtherClicked.bind(this)}
            style={touchableStyle}
          >
            <Image 
              resizeMode="stretch"
              style={imageStyle}
              source={Icons.filterOtherSelected}
            />
            <Text style={{ color: '#21A6E2' }}>Lainnya</Text>          
          </TouchableOpacity>
        </View>
      );  
    }

    return (
      <View style={buttonStyle}>
        <TouchableOpacity 
          onPress={this.onOtherClicked.bind(this)}
          style={touchableStyle}
        >
          <Image 
            resizeMode="stretch"
            style={imageStyle}
            source={Icons.filterOtherUnselected}
          />
          <Text style={{ color: '#989696' }}>Lainnya</Text>          
        </TouchableOpacity>
      </View>          
    );
  }

  renderFilter() {
    const { filterStyle } = styles;
    const { filterOpenStatus } = this.props.maps;
    
    // debug
    // console.log(maps);
    if (filterOpenStatus) {
      return (
        <View style={filterStyle}>
          {this.renderTour()}
          {this.renderHotel()}
          {this.renderTransportation()}
          {this.renderCulinary()}
          {this.renderOther()}
        </View>    
      );
    }        
  }

  render() {
    const { viewStyle, textStyle } = styles;
    return (
      <View>
        <View style={viewStyle}>
          <Text style={textStyle}>Peta YourBandung</Text>
          <TouchableOpacity onPress={this.onButtonPress.bind(this)}>
            <Text style={textStyle}>...</Text>
          </TouchableOpacity>
        </View>
        {this.renderFilter()}
      </View>
    );
  }
}

const mapStateToProps = state => {
  // debug
  // console.log(state);

  const { maps } = state;
  return { maps };
};

export default connect(mapStateToProps, { 
  openCloseFilter,
  setFilter
})(MapsHeader);
