import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

//
import Icons from '../../resources/icons';
import { CardSection, Button } from '../../shared_components';

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  thumbnailContainerStyle: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  thumbnailStyle: {
    height: 140,
    width: 150,
    borderRadius: 10
  }
};

class MarkerDetails extends Component {

  onButtonPress() {
    // const { selectedMarker } = this.props;    
    this.props.navigator.push({
      screen: 'yourbandung.MapsPartnerDetails',
      title: 'Partner Details'       
    });

    // debug
    console.log(this.props.selectedMarker);
  }

  render() {
    const {
      containerStyle, 
      thumbnailStyle 
    } = styles;    
  
    const { imageDetail, title, address, openHours } = this.props.selectedMarker;

    return (
      <View style={containerStyle}>
        <CardSection style={{ marginBottom: 10, height: 150 }}>
          <View style={{ flex: 4, borderColor: 'black' }}>
            <Image
              style={thumbnailStyle}
              source={{ uri: imageDetail }}
            />
          </View>     
          <View style={{ flex: 5, paddingLeft: 15, paddingRight: 15 }}>
            <Text numberOfLines={1} style={{ fontWeight: 'bold' }}>{title}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 15 }}>
              <Image
                style={{ width: 12, height: 12, marginRight: 3 }}
                source={Icons.location}              
              />
              <Text numberOfLines={1}>{address}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 15 }}>
              <Image
                style={{ width: 12, height: 12, marginRight: 3 }}
                source={Icons.clock}              
              />
              <Text numberOfLines={1}>{openHours}</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'column' }}>
              <View style={{ height: 35 }}>
                <Button onPress={this.onButtonPress.bind(this)}>
                  See More
                </Button>
              </View>
            </View>
          </View>                         
        </CardSection>        
      </View>
    );
  }
}

export default MarkerDetails;

