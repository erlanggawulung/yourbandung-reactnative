import React, { Component } from 'react';
import { 
  View,
  StyleSheet
 } from 'react-native';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import Permissions from 'react-native-permissions';

//
import MapsHeader from './MapsHeader';
import TabBar from '../_global/TabBar';
import { 
  setCurrentLocation, 
  selectMarker, 
  unSelectMarker,
  markersFetch
 } from '../../actions';
import Icons from '../../resources/icons';
import MarkerDetails from './MarkerDetails';

class Maps extends Component {
  state = { showModal: false }

  componentDidMount() {
    this.props.markersFetch();

    Permissions.check('location')
    .then(response => {
      if (response === 'authorized') {
        this.props.setCurrentLocation();        
      } else {
        Permissions.request('location')
        .then(requestResponse => {
          if (requestResponse === 'authorized') {
            this.props.setCurrentLocation();
          }
        });
      }
      // debug
      // console.log(response);
    });
  }

  onPressMarker(marker) {
    this.props.selectMarker(marker);    
    
    // debug
    // console.log('marker pressed!');
    // console.log(marker);
  }

  onPressMap(e) {
    if (e.nativeEvent.action === 'press') {
      this.props.unSelectMarker();

      //debug
      // console.log('map pressed!');
    }
  }

  renderMarkerDetail() {
    const { selectedMarker } = this.props.maps;
    // debug
    // console.log(selectedMarker);
      
    if (selectedMarker.key > 0) {
      return <MarkerDetails selectedMarker={selectedMarker} navigator={this.props.navigator} />;
    }
  }

  renderTourMarkers() {
    const { selectedMarker } = this.props.maps;
    const list = this.props.maps.placeList.tour;
    const filterStatus = this.props.maps.categoryStatusList.tour;
    
    if (filterStatus) {
      return (
        <View>
          {list.map((o, index) => (
              <MapView.Marker
                coordinate={{ 
                  latitude: o.coordinate.latitude, 
                  longitude: o.coordinate.longitude 
                }}
                key={index}
                image={selectedMarker === o ? o.imagePress : o.imageUnPress}
                onPress={() => this.onPressMarker(o)}
              />
            )
          )}
        </View>
      );
    }    
  }

  renderHotelMarkers() {
    const { selectedMarker } = this.props.maps;
    const list = this.props.maps.placeList.hotel;
    const filterStatus = this.props.maps.categoryStatusList.hotel;
    
    if (filterStatus) {
      return (
        <View>
          {list.map((o, index) => (
              <MapView.Marker
                coordinate={{ 
                  latitude: o.coordinate.latitude, 
                  longitude: o.coordinate.longitude 
                }}
                key={index}
                image={selectedMarker === o ? o.imagePress : o.imageUnPress}
                onPress={() => this.onPressMarker(o)}
              />
            )
          )}
        </View>
      );
    }    
  }

  renderCulinaryMarkers() {
    const { selectedMarker } = this.props.maps;
    const list = this.props.maps.placeList.culinary;
    const filterStatus = this.props.maps.categoryStatusList.culinary;
    
    if (filterStatus) {
      return (
        <View>
          {list.map((o, index) => (
              <MapView.Marker
                coordinate={{ 
                  latitude: o.coordinate.latitude, 
                  longitude: o.coordinate.longitude 
                }}
                key={index}
                image={selectedMarker === o ? o.imagePress : o.imageUnPress}
                onPress={() => this.onPressMarker(o)}
              />
            )
          )}
        </View>
      );
    }    
  }

  renderTransportationMarkers() {
    const { selectedMarker } = this.props.maps;
    const list = this.props.maps.placeList.transportation;
    const filterStatus = this.props.maps.categoryStatusList.transportation;
    
    if (filterStatus) {
      return (
        <View>
          {list.map((o, index) => (
              <MapView.Marker
                coordinate={{ 
                  latitude: o.coordinate.latitude, 
                  longitude: o.coordinate.longitude 
                }}
                key={index}
                image={selectedMarker === o ? o.imagePress : o.imageUnPress}
                onPress={() => this.onPressMarker(o)}
              />
            )
          )}
        </View>
      );
    }    
  }

  renderOtherMarkers() {
    const { selectedMarker } = this.props.maps;
    const list = this.props.maps.placeList.other;
    const filterStatus = this.props.maps.categoryStatusList.other;
    
    if (filterStatus) {
      return (
        <View>
          {list.map((o, index) => (
              <MapView.Marker
                coordinate={{ 
                  latitude: o.coordinate.latitude, 
                  longitude: o.coordinate.longitude 
                }}
                key={index}
                image={selectedMarker === o ? o.imagePress : o.imageUnPress}
                onPress={() => this.onPressMarker(o)}
              />
            )
          )}
        </View>
      );
    }    
  }

  render() {
    // debug
    // console.log(this.props);

    const { containerStyle, cardStyle } = styles;
    const { map } = mapStyles;
    
    const { currentPosition, userPosition } = this.props.maps;
    
    // debug
    // console.log(userPosition);
    
    return (
      <View style={containerStyle}>
        <MapsHeader />      
        <View style={cardStyle}>
          <MapView
            style={map}
            region={currentPosition}
            onPress={(e) => this.onPressMap(e)}
          >
            {this.renderTourMarkers()}
            {this.renderHotelMarkers()}
            {this.renderCulinaryMarkers()}
            {this.renderTransportationMarkers()}
            {this.renderOtherMarkers()}
            <MapView.Marker 
              coordinate={{ 
                latitude: userPosition.coordinate.latitude, 
                longitude: userPosition.coordinate.longitude 
              }}
              title={'You are here'}
              image={Icons.myLocation}
              onPress={() => this.onPressMarker(userPosition)}              
            />
          </MapView>
          {this.renderMarkerDetail()}          
        </View>        
        <TabBar
          navigator={this.props.navigator}
          app={this.props.app}
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    flexDirection: 'column'
  },
  textStyle: {
    fontSize: 20
  },
  cardStyle: {
    flex: 1,    
    // height: wHeight - tabBarHeight
  }
};

const mapStyles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const mapStateToProps = state => {
  const { app, maps } = state;

  // debug
  // console.log(maps);
  
  return { app, maps };
};

export default connect(mapStateToProps, { 
  setCurrentLocation,
  selectMarker,
  unSelectMarker,
  markersFetch
})(Maps);
