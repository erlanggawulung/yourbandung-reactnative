import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { FloatingAction } from 'react-native-floating-action';


//
import TabBar from '../_global/TabBar';
import SubVoucherList from './SubVoucherList';
import { vouchersFetch } from '../../actions';
import Icons from '../../resources/icons';

const hHeight = Dimensions.get('window').height;

class Voucher extends Component {
  componentWillMount() {
    this.props.vouchersFetch(10000, this.props.filterPrice);    
  }

  onFilterPress() {
    this.props.navigator.resetTo({
      screen: 'yourbandung.VoucherFilter',
      title: 'Filter'       
    });
  }

  render() {
    const { containerStyle, subContainerStyle } = styles;
    const actions = [{
      text: 'Filter',
      icon: Icons.filter,
      name: 'filter',
      position: 1
    }];

    return (
      <View style={containerStyle}>
        <View style={subContainerStyle}>
          <ScrollableTabView
            initialPage={0}
            tabBarActiveTextColor='#F27E0B'
            tabBarUnderlineStyle={{ backgroundColor: '#F27E0B' }}
            renderTabBar={() => <ScrollableTabBar activeTab={0} />}
          >
            <SubVoucherList 
              tabLabel="All" 
              navigator={this.props.navigator}
              voucherList={this.props.all}
              loading={this.props.loading}
            />
            <SubVoucherList 
              tabLabel="Kuliner" 
              navigator={this.props.navigator}
              voucherList={this.props.culinary}
              loading={this.props.loading}
            />
            <SubVoucherList 
              tabLabel="Wisata" 
              navigator={this.props.navigator}
              voucherList={this.props.tour}
              loading={this.props.loading}
            />
            <SubVoucherList 
              tabLabel="Hotel" 
              navigator={this.props.navigator}
              voucherList={this.props.hotel}
              loading={this.props.loading}
            />
            <SubVoucherList 
              tabLabel="Lainnya" 
              navigator={this.props.navigator}
              voucherList={this.props.other}
              loading={this.props.loading}
            />          
          </ScrollableTabView>
          <FloatingAction
            actions={actions}
            position='right'
            distanceToEdge={20}
            buttonColor='#F27E0B'
            overrideWithAction
            onPressItem={this.onFilterPress.bind(this)}
          />        
        </View>
          <TabBar
            navigator={this.props.navigator}
            app={this.props.app}
          />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column'
  },
  subContainerStyle: {
    height: hHeight - 140,
    flexDirection: 'column'
  },
  textStyle: {
    fontSize: 20
  },
  cardStyle: {
    // height: wHeight - tabBarHeight
  },
  thumbnailStyle: {
    height: 140,
    width: 150,
    borderRadius: 10
  }
};

const mapStateToProps = state => {
  const { app } = state;
  const { loading, filterPrice } = state.voucher;
  const { all, tour, culinary, transportation, hotel, other } = state.voucher.voucherList;

  // debug
  // console.log(state.voucher.voucherList);

  return { app, all, tour, culinary, transportation, hotel, other, loading, filterPrice };
};

export default connect(mapStateToProps, { vouchersFetch })(Voucher);
