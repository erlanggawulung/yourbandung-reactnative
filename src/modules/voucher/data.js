export const vouchers = [
  {
      id: 1,
      merchant_id: 1,
      deal_name: '[PROMO OMLEK 2018] Sensation Buffet',
      star_rate: '5.0',
      is_get_one: 0,
      price_after: 108000,
      price_before: 188000,
      discount_percentage: '43.00',
      merchant_name: 'Jasmine Lounge',
      category: 'Kuliner',
      photo: 'http://127.0.0.1:8000/api/map_image?url=app/public/deal/no-image-deal.png'
  },
  {
      id: 2,
      merchant_id: 1,
      deal_name: 'Coffe and Snack fromExcelso Coffee',
      star_rate: '4.0',
      is_get_one: 1,
      price_after: 35000,
      price_before: 65500,
      discount_percentage: '47.00',
      merchant_name: 'Jasmine Lounge',
      category: 'Kuliner',
      photo: 'http://127.0.0.1:8000/api/map_image?url=app/public/deal/no-image-deal.png'
  },
  {
      id: 3,
      merchant_id: 1,
      deal_name: 'Special Year End promo, All you can eat dimsum',
      star_rate: '5.0',
      is_get_one: 0,
      price_after: 58000,
      price_before: 90000,
      discount_percentage: '36.00',
      merchant_name: 'Jasmine Lounge',
      category: 'Kuliner',
      photo: 'http://127.0.0.1:8000/api/map_image?url=app/public/deal/no-image-deal.png'
  }
];
