import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';

//
import {
  get619RatioHeight,
  Card,
  CardSection,
  Spinner,
  Button
} from '../../shared_components';
import { getVoucherDetails } from '../../actions';
import { toIDR } from '../../utils/textRenderer';
import Icons from '../../resources/icons';
import Images from '../../resources/images';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth);

const styles = StyleSheet.create({
  wrapper: {
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#f8f8f8'
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  titleStyle: {
    fontSize: 17,
    color: '#222222'
  },
  priceAfterStyle: {
    fontSize: 13,
    color: '#FFA619'
  },
  priceBeforeStyle: {
    fontSize: 11,
    color: '#989696',
    textDecorationLine: 'line-through'
  },
  sliderImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dealImageStyle: {
    height: contentHeight / 2,
    width: wWidth / 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

class VoucherDetails extends Component {    
  static navigatorButtons = {
    rightButtons: [],
    leftButtons: [
      {
        icon: Images.leftArrow, // for icon button, provide the local image asset name
        id: 'back_to_voucher_list' // id for this button, given in onNavigatorEvent(event) to help
      }      
    ]
  };

  constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount() {
    const { selectedVoucher } = this.props.voucher;

    this.props.getVoucherDetails(selectedVoucher);
  }

  onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
    if (event.type === 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id === 'back_to_voucher_list') {
        // debug
        // console.log('left add pressed');
        this.props.navigator.resetTo({
          screen: 'yourbandung.Voucher',
          title: 'YourBandung X YourWeekdays'      
        });
      }
    }
  }

  onBuyPress() {
    this.props.navigator.push({
      screen: 'yourbandung.VoucherConfirm',
      title: 'Confirm Details'       
    });
  }

  renderRedeemsAt() {
    const { voucherOutlets } = this.props.voucher;
    return (
        <CardSection
          style={{ 
            flexDirection: 'column', 
            flex: 1,
            paddingBottom: 10 
          }}
        >
          <Text>
            Redeem Deals At
          </Text>
          <ScrollView style={{ flexDirection: 'row' }} horizontal>
            {voucherOutlets.map((value, index) => (
              <Card key={index} style={{ marginBottom: 15, width: wWidth / 2 }}>
                <Image
                  style={styles.dealImageStyle} 
                  source={{ uri: value.photo }} 
                />
                <View style={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5 }}>
                  <Text>
                    {value.name}
                  </Text>
                  <Text numberOfLines={2}>
                    {value.address}
                  </Text>                  
                </View>    
              </Card>
            ))}  
          </ScrollView>
        </CardSection>      
    );
  }

  renderMoreDeals() {
    const merchantName = this.props.voucher.voucherDetails.merchant_name;    
    const { voucherRelateds } = this.props.voucher;
    return (
        <CardSection
          style={{ 
            flexDirection: 'column', 
            flex: 1,
            paddingBottom: 10 
          }}
        >
          <Text>
            More Deals at {merchantName}
          </Text>
          <ScrollView style={{ flexDirection: 'row' }} horizontal>
            {voucherRelateds.map((value, index) => (
              <Card key={index} style={{ marginBottom: 15, width: wWidth / 2 }}>
                <Image
                  style={styles.dealImageStyle} 
                  source={{ uri: value.photo }} 
                />
                <View style={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5 }}>
                  <Text>
                    {value.deal_name}
                  </Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={styles.priceBeforeStyle}>{toIDR(value.price_before)}</Text>
                    <Text style={styles.priceAfterStyle}>{toIDR(value.price_after)}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                      style={{ width: 12, height: 12 }}
                      source={Icons.star}              
                    />
                    <Text>{value.star_rate}</Text>
                  </View>                  
                </View>    
              </Card>
            ))}  
          </ScrollView>
        </CardSection>      
    );
  }

  renderSimilarDeals() {
    const { voucherSimilars } = this.props.voucher;
    return (
        <CardSection
          style={{ 
            flexDirection: 'column', 
            flex: 1,
            paddingBottom: 10 
          }}
        >
          <Text>
            Similar Deals
          </Text>
          <ScrollView style={{ flexDirection: 'row' }} horizontal>
            {voucherSimilars.map((value, index) => (
              <Card key={index} style={{ marginBottom: 15, width: wWidth / 2 }}>
                <Image
                  style={styles.dealImageStyle} 
                  source={{ uri: value.photo }} 
                />
                <View style={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5 }}>
                  <Text>
                    {value.deal_name}
                  </Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={styles.priceBeforeStyle}>{toIDR(value.price_before)}</Text>
                    <Text style={styles.priceAfterStyle}>{toIDR(value.price_after)}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                      style={{ width: 12, height: 12 }}
                      source={Icons.star}              
                    />
                    <Text>{value.star_rate}</Text>
                  </View>                  
                </View>    
              </Card>
            ))}  
          </ScrollView>
        </CardSection>      
    );
  }  

  render() {
    if (this.props.voucher.loading) {
      return <Spinner size='large' />;      
    }

    const dealName = this.props.voucher.voucherDetails.deal_name;
    const starRate = this.props.voucher.voucherDetails.star_rate;
    const merchantName = this.props.voucher.voucherDetails.merchant_name;
    const voucherDetails = this.props.voucher.voucherDetails.detail;
    const { availability } = this.props.voucher.voucherDetails;
    // const priceAfter = this.props.voucher.voucherDetails.price_after;
    // const priceBefore = this.props.voucher.voucherDetails.price_before;
    const voucherImages = this.props.voucher.voucherImages;
    const voucherReviews = this.props.voucher.voucherReviews;

    // debug
    // console.log(contentHeight);
    // console.log(partnerDeals[0]);

    return (
      <View style={styles.containerStyle}>
        <Card>
          <CardSection style={{ height: contentHeight }}>
            <Swiper 
              style={styles.wrapper} 
              showsButtons={false}
              autoplay
              autoplayTimeout={4}
              loop
            >
              {voucherImages.map((image, index) => (
                <View key={index} style={styles.slide1}>
                  <Image
                    style={styles.sliderImageStyle} 
                    source={{ uri: image.photo }} 
                  />
                </View>
              ))}              
            </Swiper>
          </CardSection>
          <CardSection 
            style={{ 
              flexDirection: 'column', 
              height: 80,
              paddingLeft: 15,
              paddingRight: 15
           }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 15 }}>
              <Image
                style={{ width: 12, height: 12, marginRight: 3 }}
                source={Icons.star}              
              />
              <Text>{starRate}</Text>
            </View>            
            <Text style={styles.titleStyle} numberOfLines={1}>
              {dealName}
            </Text>
            <Text style={styles.titleStyle}>
              {merchantName}
            </Text>            
          </CardSection>
        </Card>
        <Card style={{ flex: 1 }}>
          <ScrollableTabView
            initialPage={0}
            tabBarActiveTextColor='#F27E0B'
            tabBarUnderlineStyle={{ backgroundColor: '#F27E0B' }}
            renderTabBar={() => <DefaultTabBar activeTab={0} />}
          >
            <ScrollView tabLabel='Information'>
              <CardSection style={{ flexDirection: 'column', paddingRight: 5, height: 70 }}>
                <Text>
                  Detail Deals
                </Text>
                <Text>
                  {voucherDetails}
                </Text>
              </CardSection>
              {this.renderRedeemsAt()}
              <CardSection style={{ flexDirection: 'column', paddingRight: 5, flex: 1 }}>
                <Text>
                  Availability
                </Text>
                <Text>
                  {availability}
                </Text>
              </CardSection>
            </ScrollView>
            <ScrollView tabLabel='Reviews'>
              {voucherReviews.map((value, index) => (
                <CardSection 
                  key={index} 
                  style={{ flexDirection: 'column', paddingRight: 5, flex: 1 }}
                >
                  <Text>
                    {value.name}
                  </Text>
                  <Text>
                    {value.review}
                  </Text>
                </CardSection>
              ))}                
            </ScrollView>
            <ScrollView tabLabel='Related'>
              {this.renderMoreDeals()}
              {this.renderSimilarDeals()}
            </ScrollView>
          </ScrollableTabView>
          <CardSection style={{ height: 50 }}>
            <Button onPress={this.onBuyPress.bind(this)}>
              Buy
            </Button>
          </CardSection>
        </Card>        
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { voucher } = state;

  // debug
  // console.log(voucher);

  return { voucher };
};

export default connect(mapStateToProps, { getVoucherDetails })(VoucherDetails);

