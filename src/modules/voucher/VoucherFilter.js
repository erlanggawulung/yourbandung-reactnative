import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Slider } from 'react-native';
import { connect } from 'react-redux';

//
// import Images from '../../resources/images';
import TabBar from '../_global/TabBar';
import { CardSection, Button, Card } from '../../shared_components';
import Icons from '../../resources/icons';
import { 
  voucherSetStarFilter, 
  voucherSetPrice,
  voucherResetFilter,
  voucherFilter
} from '../../actions';
import { toIDR } from '../../utils/textRenderer';

// const minPrice = 10000;
// const maxPrice = 500000;

class VoucherFilter extends Component { 
  onReset() {
    this.props.voucherResetFilter();

    // this.props.navigator.resetTo({
    //   screen: 'yourbandung.Voucher',
    //   title: 'YourBandung X YourWeekdays'      
    // });
  }

  onFilter() {
    // const { voucher } = this.props;

    // this.props.voucherFilter(minPrice, this.props);

    this.props.navigator.resetTo({
      screen: 'yourbandung.Voucher',
      title: 'YourBandung X YourWeekdays'      
    });
  }

  onStarClicked(index) {
    const { starStatus } = this.props.voucher;
    this.props.voucherSetStarFilter(starStatus, index);
  }

  renderOneStar() {
    const { starStatus } = this.props.voucher;
    let starIcon = Icons.star;
    if (!starStatus[1]) {
      starIcon = Icons.greyStar;
    }

    return (
      <Card>
        <TouchableOpacity
          onPress={() => this.onStarClicked(1)}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
        </TouchableOpacity>
      </Card>
    );    
  }

  renderTwoStars() {
    const { starStatus } = this.props;
    let starIcon = Icons.star;
    if (!starStatus[2]) {
      starIcon = Icons.greyStar;
    }

    return (
      <Card>
        <TouchableOpacity
          onPress={() => this.onStarClicked(2)}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
        </TouchableOpacity>
      </Card>
    );
  }

  renderThreeStars() {
    const { starStatus } = this.props;
    let starIcon = Icons.star;
    if (!starStatus[3]) {
      starIcon = Icons.greyStar;
    }

    return (
      <Card>
        <TouchableOpacity
          onPress={() => this.onStarClicked(3)}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
        </TouchableOpacity>
      </Card>
    );
  }

  renderFourStars() {
    const { starStatus } = this.props;
    let starIcon = Icons.star;
    if (!starStatus[4]) {
      starIcon = Icons.greyStar;
    }

    return (
      <Card>
        <TouchableOpacity
          onPress={() => this.onStarClicked(4)}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
        </TouchableOpacity>
      </Card>
    );
  }
  
  renderFiveStars() {
    const { starStatus } = this.props;
    let starIcon = Icons.star;
    if (!starStatus[5]) {
      starIcon = Icons.greyStar;
    }

    return (
      <Card>
        <TouchableOpacity
          onPress={() => this.onStarClicked(5)}
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
          <Image
            style={{ width: 20, height: 20 }}
            source={starIcon}              
          />
        </TouchableOpacity>
      </Card>
    );
  }

  render() {
    const { minPrice, maxPrice } = this.props.voucher;
    
    // debug
    // console.log(starStatus);

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between'
        }}
      >
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'flex-start'
          }}
        >
          <CardSection
            style={{
              flexDirection: 'column',
              height: 65
            }}
          >
            <Text>Rate</Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center'
              }}
            >              
              {this.renderOneStar()}
              {this.renderTwoStars()}
              {this.renderThreeStars()}
              {this.renderFourStars()}
              {this.renderFiveStars()}
            </View>
          </CardSection>
          <CardSection
            style={{
              flexDirection: 'column',
              height: 65
            }}
          >
            <Text>Price</Text>
            <Slider
              style={{ 
                flex: 1,
                marginRight: 15,
                marginLeft: 15 
              }}
              step={1}
              value={this.props.filterPrice}
              minimumValue={minPrice}
              maximumValue={maxPrice}
              onValueChange={val => this.props.voucherSetPrice(val)}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between' 
              }}
            >
              <Text>{toIDR(minPrice)}</Text>
              <Text>{toIDR(this.props.filterPrice)}</Text>
              <Text>{toIDR(maxPrice)}</Text>
            </View>
          </CardSection>
        </View>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'flex-end'
          }}
        >
          <CardSection
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 5
            }}
          >
            <Button
              style={{
                backgroundColor: '#989696',
                borderColor: '#989696',
                marginLeft: 3,
                marginRight: 3
              }}
              onPress={this.onReset.bind(this)}
            >
              Reset
            </Button>
            <Button
              style={{
                backgroundColor: '#F27E0B',
                borderColor: '#F27E0B',
                marginLeft: 3,
                marginRight: 3
              }}
              onPress={this.onFilter.bind(this)}
            >
              Filter
            </Button>
          </CardSection>
          <TabBar
            navigator={this.props.navigator}
            app={this.props.app}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { app, voucher } = state;
  const { starStatus, filterPrice } = state.voucher;

  // debug
  // console.log(state.voucher);

  return { app, voucher, starStatus, filterPrice };
};

export default connect(mapStateToProps, { 
  voucherSetStarFilter, 
  voucherSetPrice,
  voucherResetFilter,
  voucherFilter
 })(VoucherFilter);

