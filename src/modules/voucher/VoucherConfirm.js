import React, { Component } from 'react';
import {
  View,
  // ScrollView,
  Text,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import { connect } from 'react-redux';

//
import {
  get619RatioHeight,
  Card,
  CardSection,
  // Spinner,
  Button
} from '../../shared_components';
import { toIDR } from '../../utils/textRenderer';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth);

const styles = StyleSheet.create({
  wrapper: {
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#f8f8f8'
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  titleStyle: {
    fontSize: 17,
    color: '#222222'
  },
  priceAfterStyle: {
    fontSize: 13,
    color: '#FFA619'
  },
  priceBeforeStyle: {
    fontSize: 13,
    color: '#989696',
    textDecorationLine: 'line-through'
  },
  sliderImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dealImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

class VoucherConfirm extends Component {
  onCheckOut() {
    // debug
    console.log('check out');
  }

  render() {
    const { voucherDetails } = this.props.voucher;
    // console.log(voucherDetails);
    
    return (
      <View style={styles.containerStyle}>
        <Card>
          <CardSection
            style={{ 
              justifyContent: 'center', 
              alignItems: 'center', 
              height: contentHeight,
              padding: 1  
            }}
          >
            <Image
              style={styles.sliderImageStyle} 
              source={{ uri: voucherDetails.photo }} 
            />
          </CardSection>
          <CardSection
            style={{ 
              flexDirection: 'column', 
              height: 80,
              paddingLeft: 15,
              paddingRight: 15
           }}
          >
            <Text>{voucherDetails.deal_name}</Text>
            <Text>Valid until {voucherDetails.expired_date}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.priceBeforeStyle}>{toIDR(voucherDetails.price_before)}</Text>
              <Text style={styles.priceAfterStyle}>{toIDR(voucherDetails.price_after)}</Text>
            </View>
          </CardSection>
        </Card>
        <Card>
          <CardSection style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={{ size: 18 }}>Quantity</Text>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ size: 18 }}>-</Text>
                <Text style={{ size: 15 }}>1</Text>
                <Text style={{ size: 18 }}>+</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text>Total Price</Text>
              <Text style={styles.priceAfterStyle}>{toIDR(voucherDetails.price_after)}</Text>
            </View>
          </CardSection>
        </Card>
        <Card style={{ marginBottom: 10, height: 50 }}>
          <Button
            style={{ backgroundColor: '#FFA619', borderColor: '#FFA619' }} 
            onPress={this.onCheckOut.bind(this)}
          >
            CHECK OUT
          </Button>
        </Card>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { voucher } = state;
  return { voucher };
};

export default connect(mapStateToProps, null)(VoucherConfirm);
