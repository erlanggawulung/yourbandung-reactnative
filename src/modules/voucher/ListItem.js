import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';

//
// import Images from '../../resources/images';
import { get619RatioHeight, CardSection, Card } from '../../shared_components';
import { toIDR } from '../../utils/textRenderer';
import { voucherSelect } from '../../actions';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth);

class ListItem extends Component {
  onRowPress() {
    this.props.voucherSelect(this.props.voucher);

    // const { title } = this.props.news;    
    this.props.navigator.resetTo({
      screen: 'yourbandung.VoucherDetails',
      title: 'Voucher Details'   
    });

    //debug
    // console.log('pressed!');
    // console.log(this.props);
  }

  render() {    
    const dealName = this.props.voucher.deal_name;
    const merchantName = this.props.voucher.merchant_name;
    const priceAfter = this.props.voucher.price_after;
    const priceBefore = this.props.voucher.price_before;
    const { photo } = this.props.voucher;

    const { titleStyle, publishedStyle, priceAfterStyle, priceBeforeStyle } = styles;
    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>          
          <Card>
            <CardSection 
              style={{ 
                  justifyContent: 'center', 
                  alignItems: 'center', 
                  height: 200,
                  padding: 1  
                }}
            >
              <Image
                style={styles.sliderImageStyle} 
                source={{ uri: photo }} 
              />
            </CardSection>
            <CardSection 
            style={{ 
              height: 60, 
              justifyContent: 'center', 
              flexDirection: 'column',
              paddingLeft: 15,
              paddingRight: 15
              }}
            >
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text numberOfLines={1} style={titleStyle}>
                  { dealName.substring(0, 20) }...
                </Text>
                <Text style={priceBeforeStyle}>
                  { toIDR(priceBefore) }
                </Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={publishedStyle}>
                  { merchantName }
                </Text>
                <Text style={priceAfterStyle}>
                  { toIDR(priceAfter) }
                </Text>
              </View>
            </CardSection>   
          </Card>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 15,
    color: '#222222',
    fontWeight: 'bold'
  },
  publishedStyle: {
    fontSize: 13,
    color: '#05CC2C'
  },
  priceAfterStyle: {
    fontSize: 13,
    color: '#FFA619'
  },
  priceBeforeStyle: {
    fontSize: 11,
    color: '#989696',
    textDecorationLine: 'line-through'
  },
  sliderImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

const mapStateToProps = state => {
  const { app } = state;
  // const voucherReducers = state.voucher;
  
  // debug
  // console.log(voucherReducers);

  return { app };
};

export default connect(mapStateToProps, { voucherSelect })(ListItem);
