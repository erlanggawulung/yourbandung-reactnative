import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';

//
import {
  get619RatioHeight,
  Card,
  CardSection,
  Spinner
} from '../../shared_components';
import { getPartnerDetails } from '../../actions';
import { toIDR } from '../../utils/textRenderer';
import Icons from '../../resources/icons';
import Images from '../../resources/images';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth);

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  titleStyle: {
    fontSize: 17,
    color: '#222222'
  },
  priceAfterStyle: {
    fontSize: 13,
    color: '#FFA619'
  },
  priceBeforeStyle: {
    fontSize: 11,
    color: '#989696',
    textDecorationLine: 'line-through'
  },
  sliderImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dealImageStyle: {
    height: contentHeight / 2,
    width: wWidth / 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

class PartnerDetails extends Component {    
  static navigatorButtons = {
    rightButtons: [],
    leftButtons: [
      {
        icon: Images.leftArrow, // for icon button, provide the local image asset name
        id: 'back_to_partner_list' // id for this button, given in onNavigatorEvent(event) to help
      }      
    ]
  };

  constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount() {
    const { selectedMarker } = this.props.maps;

    this.props.getPartnerDetails(selectedMarker);
  }

  onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
    if (event.type === 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id === 'back_to_partner_list') {
        // debug
        // console.log('left add pressed');
        this.props.navigator.resetTo({
          screen: 'yourbandung.Partner',
          title: 'YourBandung X YourWeekdays'      
        });
      }
    }
  }

  renderVoucherList() {
    const { partnerDeals } = this.props.maps;
    return (
      <Card 
        style={{ 
          marginBottom: 10 
        }}
      >        
        <View
          style={{ 
            flexDirection: 'column', 
            flex: 1,
            paddingLeft: 15,
            paddingRight: 15,
            paddingTop: 10,
            paddingBottom: 10 
          }}
        >
          <Text style={styles.titleStyle}>
            Voucher
          </Text>
          <ScrollView style={{ flexDirection: 'row' }} horizontal>
            {partnerDeals.map((deal, index) => (
              <Card key={index} style={{ marginBottom: 15, width: wWidth / 2 }}>
                <Image
                  style={styles.dealImageStyle} 
                  source={{ uri: deal.photo }} 
                />
                <View style={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5 }}>
                  <Text style={styles.titleStyle} numberOfLines={2}>
                    {deal.deal_name}
                  </Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={styles.priceBeforeStyle}>{toIDR(deal.price_before)}</Text>
                    <Text style={styles.priceAfterStyle}>{toIDR(deal.price_after)}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                      style={{ width: 12, height: 12 }}
                      source={Icons.star}              
                    />
                    <Text>{deal.star_rate}</Text>
                  </View>
                </View>    
              </Card>
            ))}  
          </ScrollView>
        </View>
      </Card>
    );
  }

  renderHighlight() {
    const { partnerDetails } = this.props.maps;

    if (partnerDetails.path_highlight) {
      return (
        <Card
          style={{ 
            marginBottom: 10 
          }}
        >
          <View
            style={{ 
              flexDirection: 'column', 
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              paddingTop: 10,
              paddingBottom: 10 
            }}
          >
            <Text style={styles.titleStyle}>Highlight</Text>
            <View style={styles.slide1}>
              <Image
                style={styles.sliderImageStyle} 
                source={{ uri: partnerDetails.path_highlight }} 
              />
            </View>
          </View>
        </Card>
      );
    }    
  }

  render() {
    if (this.props.maps.partnerLoading) {
      return <Spinner size='large' />;      
    }

    const { partnerDetails, partnerImages } = this.props.maps;
    // debug
    // console.log(contentHeight);
    // console.log(partnerDeals[0]);

    return (
      <ScrollView>
        <Card>
          <CardSection style={{ height: contentHeight }}>
            <Swiper 
              style={styles.wrapper} 
              showsButtons={false}
              autoplay
              autoplayTimeout={4}
              loop
            >
              {partnerImages.map((image, index) => (
                <View key={index} style={styles.slide1}>
                  <Image
                    style={styles.sliderImageStyle} 
                    source={{ uri: image.photo }} 
                  />
                </View>
              ))}              
            </Swiper>
          </CardSection>
          <CardSection 
            style={{ 
            flexDirection: 'column', 
            height: 100,
            paddingLeft: 15,
            paddingRight: 15
           }}
          >
            <Text style={styles.titleStyle}>
              {partnerDetails.name}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 15 }}>
              <Image
                style={{ width: 12, height: 12, marginRight: 3 }}
                source={Icons.location}              
              />
              <Text numberOfLines={1}>{partnerDetails.address}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                style={{ width: 12, height: 12, marginRight: 3 }}
                source={Icons.clock}              
              />
              <Text>{partnerDetails.openHours}</Text>
            </View>            
          </CardSection>
        </Card>
        <Card>
          <View 
            style={{ 
              flexDirection: 'column', 
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              paddingTop: 10,
              paddingBottom: 10 
            }}
          >
            <Text style={styles.titleStyle}>
              Description
            </Text>
            <Text>
              {partnerDetails.description}
            </Text>
          </View>
        </Card>         
        {this.renderVoucherList()}
        {this.renderHighlight()}                    
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const { maps } = state;

  // debug
  // console.log(maps.partnerImages[0].photo);

  return { maps };
};

export default connect(mapStateToProps, { getPartnerDetails })(PartnerDetails);

