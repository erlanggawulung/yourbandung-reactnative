import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';

//
import TabBar from '../_global/TabBar';
import SubPartnerList from './SubPartnerList';
import { partnersFetch } from '../../actions';

class Partner extends Component {
  componentWillMount() {
    this.props.partnersFetch();    
  }

  render() {
    const { containerStyle } = styles;

    return (
      <View style={containerStyle}>
        <ScrollableTabView
          initialPage={0}
          tabBarActiveTextColor='#F27E0B'
          tabBarUnderlineStyle={{ backgroundColor: '#F27E0B' }}
          renderTabBar={() => <ScrollableTabBar activeTab={0} />}
        >
          <SubPartnerList 
            tabLabel="Wisata" 
            navigator={this.props.navigator}
            partnerList={this.props.tour}
            loading={this.props.loading}
          />
          <SubPartnerList 
            tabLabel="Kuliner" 
            navigator={this.props.navigator}
            partnerList={this.props.culinary}
            loading={this.props.loading}
          />
          <SubPartnerList 
            tabLabel="Hotel" 
            navigator={this.props.navigator}
            partnerList={this.props.hotel}
            loading={this.props.loading}
          />
          <SubPartnerList 
            tabLabel="Transportasi" 
            navigator={this.props.navigator}
            partnerList={this.props.transportation}
            loading={this.props.loading}
          />          
          <SubPartnerList 
            tabLabel="Lainnya" 
            navigator={this.props.navigator}
            partnerList={this.props.other}
            loading={this.props.loading}
          />
        </ScrollableTabView>
        <TabBar
          navigator={this.props.navigator}
          app={this.props.app}
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#f8f8f8'
  },
  textStyle: {
    fontSize: 20
  },
  cardStyle: {
    // height: wHeight - tabBarHeight
  }
};

const mapStateToProps = state => {
  const { app } = state;
  const { loading } = state.partner;
  const { all, tour, culinary, transportation, hotel, other } = state.partner.partnerList;

  // debug
  // console.log(state);

  return { app, all, tour, culinary, transportation, hotel, other, loading };
};

export default connect(mapStateToProps, { partnersFetch })(Partner);
