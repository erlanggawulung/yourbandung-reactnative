export const partners = [
  {
      id: 1,
      name: 'Jasmine Lounge',
      address: 'Jalan kyai gede utama no.12 Kota Bandung, Provinsi Jawa Barat, 40132',
      lat: '-6.9080880',
      long: '107.6155670',
      category: 'Kuliner',
      voucher_available: true
  },
  {
      id: 2,
      name: 'Rose Cafe',
      address: 'Jalan taman sari no.35 C Bandung, 40132',
      lat: '-6.8908620',
      long: '107.6107750',
      category: 'Kuliner',
      voucher_available: false
  },
  {
      id: 4,
      name: 'Tulip Cafe',
      address: 'Jalan cipaganti bawah no.129, Bandung Wetan, Kota Bandung',
      lat: '-6.9080880',
      long: '107.6155670',
      category: 'Wisata',
      voucher_available: false
  }
];
