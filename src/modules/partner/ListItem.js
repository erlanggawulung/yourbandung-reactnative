import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';

//
import { get619RatioHeight, CardSection, Card } from '../../shared_components';
import { 
  selectMarker
 } from '../../actions';
// import Images from '../../resources/images';

const wWidth = Dimensions.get('window').width;
const contentHeight = get619RatioHeight(wWidth);

class ListItem extends Component {
  onRowPress() {
    // this.props.newsSelected(this.props.news);
    this.props.selectMarker(this.props.partner);

    this.props.navigator.resetTo({
      screen: 'yourbandung.PartnerDetails',
      title: 'Partner Details'       
    });

    //debug
    // console.log('pressed!');
    // console.log(this.props.maps.selectedMarker);
    // console.log(this.props.partner);
  }

  render() {    
    const { title, address, imageDetail } = this.props.partner;
    const { titleStyle, publishedStyle } = styles;
    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>          
          <Card>
            <CardSection 
              style={{ 
                  justifyContent: 'center', 
                  alignItems: 'center', 
                  height: 200,
                  padding: 1  
                }}
            >
              <Image
                style={styles.sliderImageStyle} 
                source={{ uri: imageDetail }} 
              />
            </CardSection>
            <CardSection 
              style={{ 
                height: 60, 
                justifyContent: 'center', 
                flexDirection: 'column',
                paddingLeft: 15,
                paddingRight: 15 
              }}
            >
              <Text style={titleStyle}>
                { title }
              </Text>
              <Text numberOfLines={2} style={publishedStyle}>
                { address }
              </Text>
            </CardSection>   
          </Card>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 15,
    color: '#222222',
    fontWeight: 'bold'
  },
  publishedStyle: {
    fontSize: 10
  },
  sliderImageStyle: {
    height: contentHeight,
    width: wWidth,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

const mapStateToProps = state => {
  const { app, maps } = state;
  
  return { app, maps };
};

export default connect(mapStateToProps, { selectMarker })(ListItem);
