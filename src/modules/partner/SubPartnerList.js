import React, { Component } from 'react';
import { ListView } from 'react-native';

//
import ListItem from './ListItem';
import { Spinner } from '../../shared_components';

class SubPartnerList extends Component {
  componentWillMount() {
    this.createDataSource(this.props);

    // debug
    // console.log(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }

  createDataSource({ partnerList }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(partnerList);
  }

  renderRow(partner) {    
    return <ListItem partner={partner} navigator={this.props.navigator}/>;
  }
    
  render() {
    const { loading } = this.props;
    if (loading) {
      return <Spinner spinnerColor='#F27E0B' />;
    }
    return (
      <ListView
        enableEmptySections
        dataSource={this.dataSource}
        renderRow={this.renderRow.bind(this)}
      />    
    );
  }
}

export default SubPartnerList;
