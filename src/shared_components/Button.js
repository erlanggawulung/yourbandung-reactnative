import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, style, children }) => {
  const { textStyle, buttonStyle } = styles;
  return (
    <TouchableOpacity onPress={onPress} style={[buttonStyle, style]}>
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
    // paddingTop: 10,
    // paddingBottom: 10
  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#21A6E2',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#21A6E2',
    // marginLeft: 5,
    // marginRight: 5
  }
};

export { Button };
