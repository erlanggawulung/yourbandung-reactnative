import {
  Dimensions
} from 'react-native';

const standardHeight = 960;
const standardWidth = 640;
const wHeight = Dimensions.get('window').height;
const wWidth = Dimensions.get('window').width;

const get619RatioHeight = () => (parseInt((wWidth / 1.78), 10));
const getRelativeHeight = (height) => (parseInt(((height / standardHeight) * wHeight), 10));
const getRelativeWidth = (width) => (parseInt(((width / standardWidth) * wWidth), 10));

export { get619RatioHeight, getRelativeHeight, getRelativeWidth };
