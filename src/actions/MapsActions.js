import { Alert } from 'react-native';
import _ from 'lodash';
import axios from 'axios';

//
// import { markers } from '../modules/maps/data';
import {
  MAPS_FETCH_MARKERS_SUCCESS,
  MAPS_SET_CURRENT_LOCATION,
  MAPS_SELECT_MARKER,
  PARTNER_DETAIL_FETCH_SUCCESS,
  MAPS_UNSELECT_MARKER,
  MAPS_OPEN_CLOSE_FILTER,
  MAPS_SET_FILTER,
  PARTNER_DETAIL
} from '../resources/values/actionTypes';
import {
  TOUR,
  CULINARY,
  HOTEL,
  TRANSPORTATION,
  OTHER
} from '../resources/values/markerType';
import {
  API_BASE_URL
} from '../resources/values/baseUrl';

export const setCurrentLocation = () => {
  return (dispatch) => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121
        };

        dispatch({ type: MAPS_SET_CURRENT_LOCATION, payload: region });
        // debug
        // console.log(position);
      },
      (error) => {
        Alert.alert(
          'Error',
          'GPS belum diaktifkan',
          [
            { text: 'OK', onPress: () => {} }
          ]
        );
        // debug
        console.log(error);
      },
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 10e3,
      }
    );
  };
};

export const markersFetch = () => {
  return (dispatch) => {       
    const mapsUrl = `${API_BASE_URL}/maps`;
    
    axios.get(mapsUrl)
    .then((response) => {
      const markers = response.data.data.markers;
      const placeList = {
        tour: [],
        hotel: [],
        transportation: [],
        culinary: [],
        other: []
      };
  
      _.filter(markers, (marker) => {
        const newMarker = marker;
        newMarker.coordinate.longitude = parseFloat(marker.coordinate.longitude);
        newMarker.coordinate.latitude = parseFloat(marker.coordinate.latitude);
        if (newMarker.category.toLowerCase() === TOUR) {
          placeList.tour.push(newMarker);
          return newMarker;
        } else if (newMarker.category.toLowerCase() === HOTEL) {
          placeList.hotel.push(newMarker);
          return newMarker;
        } else if (newMarker.category.toLowerCase() === TRANSPORTATION) {
          placeList.transportation.push(newMarker);
          return newMarker;
        } else if (newMarker.category.toLowerCase() === CULINARY) {
          placeList.culinary.push(newMarker);
          return newMarker;
        } else if (newMarker.category.toLowerCase() === OTHER) {
          placeList.other.push(newMarker);
          return newMarker;
        }
      });

      dispatch({ type: MAPS_FETCH_MARKERS_SUCCESS, payload: placeList });
      
      //debug
      // console.log(markers[0].category.toLowerCase());
    })
    .catch(error => {
      // debug
      console.log(error);
    });
  };
};

export const selectMarker = (marker) => {
  return (dispatch) => {
    dispatch({ type: MAPS_SELECT_MARKER, payload: marker });    
  };
};

export const getPartnerDetails = (marker) => {
  return (dispatch) => {
    dispatch({ type: PARTNER_DETAIL });

    const partnerDetailUrl = `${API_BASE_URL}/detail_merchant?idMerchant=${marker.key}`;

    axios.get(partnerDetailUrl)
    .then(response => {
      const data = response.data.data;
      dispatch({ type: PARTNER_DETAIL_FETCH_SUCCESS, payload: data });

      // debug
      // console.log(data);
    })
    .catch();
  };
};

export const unSelectMarker = () => {
  return (dispatch) => dispatch({ type: MAPS_UNSELECT_MARKER });
};

export const openCloseFilter = () => {
  return (dispatch) => dispatch({ type: MAPS_OPEN_CLOSE_FILTER });
};

export const setFilter = (statusList, category) => {
  return (dispatch) => {
    const result = statusList;
    if (category === TOUR) {
      result.tour = !statusList.tour;
    } else if (category === HOTEL) {
      result.hotel = !statusList.hotel;
    } else if (category === CULINARY) {
      result.culinary = !statusList.culinary;
    } else if (category === TRANSPORTATION) {
      result.transportation = !statusList.transportation;
    } else if (category === OTHER) {
      result.other = !statusList.other;
    }

    dispatch({ type: MAPS_SET_FILTER, payload: result });

    // debug
    // console.log(result);
  };
};
