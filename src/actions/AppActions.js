import { APP_TAB_CHANGED } from '../resources/values/actionTypes';

export const tabChanged = (tab) => {
  return {
    type: APP_TAB_CHANGED,
    payload: tab
  };
};
