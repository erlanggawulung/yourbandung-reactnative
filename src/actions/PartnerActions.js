import _ from 'lodash';
import axios from 'axios';

//
// import { partners } from '../modules/partner/data';
import { 
  PARTNERS_FETCH_SUCCESS
} from '../resources/values/actionTypes';
import {
  WISATA,
  KULINER,
  HOTEL,
  TRANSPORTASI,
  LAINNYA
} from '../resources/values/partnerType';
import {
  API_BASE_URL
} from '../resources/values/baseUrl';

export const partnersFetch = () => {
  return (dispatch) => {   
    const merchantsUrl = `${API_BASE_URL}/maps`;
    
    axios.get(merchantsUrl)
    .then(response => {
      const partners = response.data.data.markers;

      const partnerList = {
        all: [],
        tour: [],
        hotel: [],
        transportation: [],
        culinary: [],
        other: []
      };

      // all
      for (let i = 0; i < partners.length; i++) {
        partnerList.all.push(partners[i]);
      }

      _.filter(partners, (partner) => {
        if (partner.category.toLowerCase() === WISATA) {
          partnerList.tour.push(partner);
        } else if (partner.category.toLowerCase() === KULINER) {
          partnerList.culinary.push(partner);
        } else if (partner.category.toLowerCase() === TRANSPORTASI) {
          partnerList.transportation.push(partner);
        } else if (partner.category.toLowerCase() === HOTEL) {
          partnerList.hotel.push(partner);
        } else if (partner.category.toLowerCase() === LAINNYA) {
          partnerList.other.push(partner);
        }
      });
      
      dispatch({ type: PARTNERS_FETCH_SUCCESS, payload: partnerList });
    })
    .catch(error => {
      // debug
      console.log(error);
    });
  };
};
