import _ from 'lodash';
import axios from 'axios';

//
// import { vouchers } from '../modules/voucher/data';
import { 
  VOUCHERS_FETCH_SUCCESS,
  VOUCHER_SET_STAR_FILTER,
  VOUCHER_SET_PRICE,
  VOUCHER_RESET_FILTER,
  VOUCHER_FILTER,
  VOUCHER_SELECT,
  VOUCHER_DETAILS,
  VOUCHER_DETAIL_FETCH_SUCCESS
} from '../resources/values/actionTypes';
import {
  WISATA,
  KULINER,
  HOTEL,
  TRANSPORTASI,
  LAINNYA
} from '../resources/values/partnerType';
import {
  API_BASE_URL
} from '../resources/values/baseUrl';

export const vouchersFetch = (minPrice, filterPrice) => {
  return (dispatch) => {  
    const vouchersUrl = `${API_BASE_URL}/vouchers`;
    
    axios.get(vouchersUrl)
    .then((response) => {
      const vouchers = response.data.data.vouchers;

      const voucherList = {
        all: [],
        tour: [],
        hotel: [],
        transportation: [],
        culinary: [],
        other: []
      };    
  
      // all
      for (let i = 0; i < vouchers.length; i++) {
        voucherList.all.push(vouchers[i]);
      }
  
      _.filter(vouchers, (voucher) => {
        if (voucher.category.toLowerCase() === WISATA) {
          voucherList.tour.push(voucher);
        } else if (voucher.category.toLowerCase() === KULINER) {
          voucherList.culinary.push(voucher);
        } else if (voucher.category.toLowerCase() === TRANSPORTASI) {
          voucherList.transportation.push(voucher);
        } else if (voucher.category.toLowerCase() === HOTEL) {
          voucherList.hotel.push(voucher);
        } else if (voucher.category.toLowerCase() === LAINNYA) {
          voucherList.other.push(voucher);
        }
      });

      // debug
      // console.log(voucherList);

      const filtered = {
        all: [],
        tour: [],
        hotel: [],
        transportation: [],
        culinary: [],
        other: []       
      };
  
      for (let i = 0; i < vouchers.length; i++) {
        if (vouchers[i].price_after >= minPrice && vouchers[i].price_after <= filterPrice) {
          filtered.all.push(vouchers[i]);
        }
      }
  
      _.filter(filtered.all, (voucher) => {
        if (voucher.category.toLowerCase() === WISATA) {
          filtered.tour.push(voucher);
        } else if (voucher.category.toLowerCase() === KULINER) {
          filtered.culinary.push(voucher);
        } else if (voucher.category.toLowerCase() === TRANSPORTASI) {
          filtered.transportation.push(voucher);
        } else if (voucher.category.toLowerCase() === HOTEL) {
          filtered.hotel.push(voucher);
        } else if (voucher.category.toLowerCase() === LAINNYA) {
          filtered.other.push(voucher);
        }
      });

      const result = {
        voucherList,
        filtered
      };

      dispatch({ type: VOUCHERS_FETCH_SUCCESS, payload: result });
    })
    .catch(error => {
      // debug
      console.log(error);
    });
  };
};

export const voucherSetStarFilter = (starStatus, index) => {
  return (dispatch) => {
    const result = starStatus.slice(0);
    result[index] = !result[index];

    dispatch({ type: VOUCHER_SET_STAR_FILTER, payload: result });
  };
};

export const voucherSetPrice = (price) => {
  return (dispatch) => {
    dispatch({ type: VOUCHER_SET_PRICE, payload: price });
  };
};

export const voucherResetFilter = () => {
  return (dispatch) => {
    dispatch({ type: VOUCHER_RESET_FILTER });
  };
};

export const voucherFilter = (minPrice, props) => {
  return (dispatch) => {
    const { originalVoucherList, filterPrice } = props.voucher;
    const vouchers = originalVoucherList.all;
    const result = {
      all: [],
      tour: [],
      hotel: [],
      transportation: [],
      culinary: [],
      other: []       
    };

    for (let i = 0; i < vouchers.length; i++) {
      if (vouchers[i].price_after >= minPrice && vouchers[i].price_after <= filterPrice) {
        result.all.push(vouchers[i]);
      }
    }

    _.filter(result.all, (voucher) => {
      if (voucher.category.toLowerCase() === WISATA) {
        result.tour.push(voucher);
      } else if (voucher.category.toLowerCase() === KULINER) {
        result.culinary.push(voucher);
      } else if (voucher.category.toLowerCase() === TRANSPORTASI) {
        result.transportation.push(voucher);
      } else if (voucher.category.toLowerCase() === HOTEL) {
        result.hotel.push(voucher);
      } else if (voucher.category.toLowerCase() === LAINNYA) {
        result.other.push(voucher);
      }
    });

    // debug
    // console.log(result);

    props.navigator.resetTo({
      screen: 'yourbandung.Voucher',
      title: 'YourBandung X YourWeekdays'      
    });

    dispatch({ type: VOUCHER_FILTER, payload: result });
  };
};

export const voucherSelect = (voucher) => {
  return (dispatch) => {
    dispatch({ type: VOUCHER_SELECT, payload: voucher });
  };
};

export const getVoucherDetails = (selectedVoucher) => {
  return (dispatch) => {
    dispatch({ type: VOUCHER_DETAILS });

    const voucherDetailsUrl = `${API_BASE_URL}/detail_voucher?idVoucher=${selectedVoucher.id}`;

    axios.get(voucherDetailsUrl)
    .then(response => {
      const data = response.data.data;
      dispatch({ type: VOUCHER_DETAIL_FETCH_SUCCESS, payload: data });

      // debug
      // console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  };
};

