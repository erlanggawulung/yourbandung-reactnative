import * as rssParser from 'react-native-rss-parser';
import _ from 'lodash';

//
import { NEWS_FETCH_SUCCESS, 
  NEWS_FETCH_LOADING,
  NEWS_SELECTED,
  NEWS_FILTERED
} from '../resources/values/actionTypes';

export const newsFetch = () => {
  return (dispatch) => {
    // fetch('http://www.nasa.gov/rss/dyn/breaking_news.rss')
    dispatch({ type: NEWS_FETCH_LOADING });

    fetch('http://yourbandung.com/feed/')
    .then((response) => response.text())
    .then((responseData) => rssParser.parse(responseData))
    .then((rss) => {
      const result = {
        semua: [],
        berita: [],
        kuliner: [],
        acara: [],
        infografis: [],
        komik: []
      };
      
      // semua
      for (let i = 0; i < rss.items.length; i++) {
        result.semua.push(rss.items[i]);
      }

      // categorize
      _.filter(rss.items, (newsItem) => {
        _.filter(newsItem.categories, (o) => {
          if (o.name.toLowerCase() === 'berita') {
            result.berita.push(newsItem);
            return newsItem;
          } else if (o.name.toLowerCase() === 'kuliner') {
            result.kuliner.push(newsItem);
            return newsItem;
          } else if (o.name.toLowerCase() === 'acara') {
            result.acara.push(newsItem);
            return newsItem;
          } else if (o.name.toLowerCase() === 'infografis') {
            result.infografis.push(newsItem);
            return newsItem;
          } else if (o.name.toLowerCase() === 'komik') {
            result.komik.push(newsItem);
            return newsItem;
          }
          return newsItem;
        });
      });

      // debug
      // console.log(rss);
      console.log(result);
      dispatch({ type: NEWS_FETCH_SUCCESS, payload: result });     
    });
  };
};

export const newsSelected = (news) => {
  return (dispatch) => {
    dispatch({ type: NEWS_SELECTED, payload: news });
  };
};

export const newsFiltered = (originNewsList, category) => { 
  const result = [];
  _.filter(originNewsList, (newsItem) => {
    _.filter(newsItem.categories, (o) => {
      if (o.name === category) {
        result.push(newsItem);
        return newsItem;
      } 
    });
  });

  // debug
  // console.log(result);

  return (dispatch) => {
    dispatch({ type: NEWS_FILTERED, payload: result });
  };
};
