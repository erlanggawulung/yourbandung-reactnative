import {
  AsyncStorage
} from 'react-native';
import axios from 'axios';

//
import { 
  NAME_CHANGED,
  EMAIL_CHANGED, 
  GENDER_CHANGED,
  PASSWORD_CHANGED,
  PASSWORD_CONFIRMATION_CHANGED, 
  LOGIN_USER,
  LOGIN_USER_SUCCESS, 
  LOGIN_USER_FAIL,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAIL,
  LOGOUT_USER
} from '../resources/values/actionTypes';
import {
  API_BASE_URL
} from '../resources/values/baseUrl';

export const nameChanged = (text) => {
  return {
    type: NAME_CHANGED,
    payload: text
  };
};

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const genderChanged = (text) => {
  return {
    type: GENDER_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const passwordConfirmationChanged = (text) => {
  return {
    type: PASSWORD_CONFIRMATION_CHANGED,
    payload: text
  };
};

export const loginUser = ({ email, password, navigator }) => {
  return (dispatch) => {
    const loginUrl = `${API_BASE_URL}/login`;
    loginUserStart(dispatch);
    axios.post(loginUrl, {
      email,
      password
    })
    .then(response => {
      loginUserSuccess(dispatch);
      try {
        AsyncStorage.setItem('auth', JSON.stringify(response.data.data));
      } catch (err) {
        // debug
        console.log(err);
      }
      navigator.resetTo({
        screen: 'yourbandung.Me',
        title: 'YourBandung X YourWeekdays'      
      });

      // debug
      // console.log(response);
      // console.log(response.data.data);
    })
    .catch(error => {
      loginUserFail(dispatch);
      
      // debug
      console.log(error);
    });                       
  };
};

export const signUpUser = ({ name, email, password, passwordConfirmation, gender, coordinate }) => {
  return (dispatch) => {
    // debug
    // console.log(password, passwordConfirmation);
    
    dispatch({
      type: SIGNUP_USER
    });

    if (password !== passwordConfirmation) {
      // debug
      // console.log('password did not match');

      dispatch({
        type: SIGNUP_USER_FAIL,
        payload: 'Password tidak sama'
      });
    } else {
      // debug
      console.log(name, email, password, passwordConfirmation, gender, coordinate);
      const signupUrl = `${API_BASE_URL}/signup`;

      axios.post(signupUrl, {
        email,
        password,
        fullname: name,
        gender,
        long: coordinate.longitude,
        lat: coordinate.latitude
      })
      .then(response => {
        dispatch({
          type: SIGNUP_USER_SUCCESS
        });

        // debug
        console.log(response);
      })
      .catch(error => {
        dispatch({
          type: SIGNUP_USER_FAIL,
          payload: 'Terjadi kesalahan'
        });
        // debug
        console.log(error);
      });               
    }          
  };
};

const loginUserStart = (dispatch) => {
  dispatch({
    type: LOGIN_USER
  });
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
};

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error
  });
};

export const getProfile = (navigator) => { 
  return (dispatch) => {
    getProfileStart(dispatch);
    AsyncStorage.getItem('auth')
    .then(value => {
      if (value) {
        const profileUrl = `${API_BASE_URL}/profile?token=${JSON.parse(value).token}`;
        axios.get(profileUrl)
        .then(response => {
          dispatch({
            type: FETCH_PROFILE_SUCCESS,
            payload: response.data.data.profile[0]
          });
        })
        .catch(error => {
          // debug
          console.log(error);
        });
      } else {
        dispatch({ type: FETCH_PROFILE_FAIL });
        navigator.resetTo({
          screen: 'yourbandung.Login',
          title: 'YourBandung X YourWeekdays'      
        });
      }
    })
    .catch(() => {
      dispatch({ type: FETCH_PROFILE_FAIL });
      navigator.resetTo({
        screen: 'yourbandung.Login',
        title: 'YourBandung X YourWeekdays'      
      });
    })
    .done();
  };
};

const getProfileStart = (dispatch) => {
  dispatch({
    type: FETCH_PROFILE  
  });
};

export const logoutUser = (navigator) => {
  return (dispatch) => {
    navigator.resetTo({
      screen: 'yourbandung.Login',
      title: 'YourBandung X YourWeekdays'      
    });

    AsyncStorage.removeItem('auth');
    
    dispatch({
      type: LOGOUT_USER
    });   
  };
};
