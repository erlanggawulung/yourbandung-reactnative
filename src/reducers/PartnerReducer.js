import { 
  PARTNERS_FETCH_SUCCESS
} from '../resources/values/actionTypes';

const INITIAL_STATE = {
  loading: false,
  partnerList: {
    all: [],
    tour: [],
    hotel: [],
    transportation: [],
    culinary: [],
    other: []
  },
  selectedPartner: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PARTNERS_FETCH_SUCCESS:
      return { 
        ...state, 
        loading: false, 
        partnerList: action.payload 
      };
    default:
      return state;
  }
};
