import { 
  NAME_CHANGED,
  EMAIL_CHANGED, 
  GENDER_CHANGED,
  PASSWORD_CHANGED,
  PASSWORD_CONFIRMATION_CHANGED, 
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
  RESET,
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAIL
} from '../resources/values/actionTypes';

const INITIAL_STATE = {
  name: '',
  email: '',
  gender: '',
  password: '',
  passwordConfirmation: '',
  user: null,
  error: '',
  loading: false,
  isLoggedIn: false,
  successMessage: '',
  jwtToken: '',
  profile: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NAME_CHANGED:
      return { ...state, name: action.payload };
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case GENDER_CHANGED:
      return { ...state, gender: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case PASSWORD_CONFIRMATION_CHANGED:
      return { ...state, passwordConfirmation: action.payload };      
    case LOGIN_USER:
      return { ...state, loading: true, error: '' };
    case SIGNUP_USER:
      return { ...state, loading: true, error: '' };
    case SIGNUP_USER_SUCCESS:
      return { ...state, loading: false, successMessage: 'User baru berhasil didaftarkan' };
    case SIGNUP_USER_FAIL:
      return { ...state, loading: false, error: action.payload, successMessage: '' };
    case LOGIN_USER_SUCCESS:
      return { 
        ...state, 
        ...INITIAL_STATE,
        user: action.payload,
        isLoggedIn: true
      };
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication Failed.', password: '', loading: false };
    case LOGOUT_USER:
      return { ...state, isLoggedIn: false };
    case RESET:
      return { ...state, INITIAL_STATE };
    case FETCH_PROFILE:
      return { ...state, loading: true };
    case FETCH_PROFILE_SUCCESS:
      return { ...state, loading: false, profile: action.payload, isLoggedIn: true };
    case FETCH_PROFILE_FAIL:
      return { ...state, loading: false, isLoggedIn: false };
    default:
      return state;
  }
};
