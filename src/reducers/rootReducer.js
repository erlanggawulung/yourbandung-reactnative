import { combineReducers } from 'redux';
import App from './AppReducer';
import News from './NewsReducer';
import Maps from './MapsReducer';
import Partner from './PartnerReducer';
import Voucher from './VoucherReducer';
import Auth from './AuthReducer';

const rootReducer = combineReducers({
  app: App,
  news: News,
  maps: Maps,
  partner: Partner,
  voucher: Voucher,
  auth: Auth
});

export default rootReducer;
