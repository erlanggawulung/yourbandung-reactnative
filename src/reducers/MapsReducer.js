import { 
  MAPS_FETCH_MARKERS_SUCCESS,
  MAPS_SET_CURRENT_LOCATION,
  MAPS_SELECT_MARKER,
  MAPS_UNSELECT_MARKER,
  PARTNER_DETAIL,
  PARTNER_DETAIL_FETCH_SUCCESS,
  MAPS_OPEN_CLOSE_FILTER,
  MAPS_SET_FILTER
} from '../resources/values/actionTypes';

const INITIAL_STATE = {
  currentPosition: {
    latitude: -6.9207889,
    longitude: 107.6096229,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121
  },
  userPosition: {
    coordinate: {
      latitude: -6.9207889,
      longitude: 107.6096229,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121
    }
  },
  placeList: {
    tour: [],
    hotel: [],
    transportation: [],
    culinary: [],
    other: []
  },
  categoryStatusList: {
    tour: true,
    hotel: true,
    transportation: true,
    culinary: true,
    other: true
  },
  selectedMarker: {},
  partnerDetails: {},
  partnerImages: [],
  partnerDeals: [],
  partnerLoading: false,
  filterOpenStatus: false  
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MAPS_FETCH_MARKERS_SUCCESS:
      return {
        ...state, 
        placeList: action.payload
      };
    case MAPS_SET_CURRENT_LOCATION:
      return { 
        ...state,
        currentPosition: action.payload,
        userPosition: {
          coordinate: action.payload
        }
      };
    case MAPS_SELECT_MARKER:
      return {
        ...state,
        selectedMarker: action.payload,
        currentPosition: {
          latitude: parseFloat(action.payload.coordinate.latitude),
          longitude: parseFloat(action.payload.coordinate.longitude),
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121
        }
      };
    case PARTNER_DETAIL:
      return {
        ...state,
        partnerLoading: true
      };
    case PARTNER_DETAIL_FETCH_SUCCESS:
      return {
        ...state,
        partnerLoading: false,
        partnerDetails: action.payload.merchant,
        partnerImages: action.payload.merchant_photos,
        partnerDeals: action.payload.deals
      };
    case MAPS_UNSELECT_MARKER:
      return {
        ...state,
        selectedMarker: {}
      };
    case MAPS_OPEN_CLOSE_FILTER:
      return {
        ...state,
        filterOpenStatus: !state.filterOpenStatus
      };
    case MAPS_SET_FILTER:
      return {
        ...state,
        categoryStatusList: action.payload
      };
    default:
      return state;
  }
};
