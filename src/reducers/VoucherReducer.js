import { 
  VOUCHERS_FETCH_SUCCESS,
  VOUCHER_SET_STAR_FILTER,
  VOUCHER_SET_PRICE,
  VOUCHER_RESET_FILTER,
  VOUCHER_FILTER,
  VOUCHER_SELECT,
  VOUCHER_DETAILS,
  VOUCHER_DETAIL_FETCH_SUCCESS
} from '../resources/values/actionTypes';

const INITIAL_STATE = {
  loading: false,
  originalVoucherList: {
    all: [],
    tour: [],
    hotel: [],
    transportation: [],
    culinary: [],
    other: []
  },
  voucherList: {
    all: [],
    tour: [],
    hotel: [],
    transportation: [],
    culinary: [],
    other: []
  },
  selectedVoucher: '',
  voucherDetails: {},
  voucherImages: [],
  voucherOutlets: [],
  voucherReviews: [],
  voucherRelateds: [],
  voucherSimilars: [],
  starStatus: [
    false,
    true,
    true,
    true,
    true,
    true
  ],
  minPrice: 10000,
  maxPrice: 500000,
  filterPrice: 500000 / 2,

};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case VOUCHERS_FETCH_SUCCESS:
      return { 
        ...state, 
        loading: false, 
        voucherList: action.payload.filtered,
        originalVoucherList: action.payload.voucherList
      };
    case VOUCHER_SET_STAR_FILTER:
      return {
        ...state,
        starStatus: action.payload
      };
    case VOUCHER_SET_PRICE:
      return {
        ...state,
        filterPrice: action.payload
      };
    case VOUCHER_RESET_FILTER:
      return {
        ...state,
        starStatus: INITIAL_STATE.starStatus,
        filterPrice: INITIAL_STATE.filterPrice
      };
    case VOUCHER_FILTER:
      return {
        ...state,
        voucherList: action.payload
      };
    case VOUCHER_SELECT:
      return {
        ...state,
        selectedVoucher: action.payload
      };
    case VOUCHER_DETAILS:
      return {
        ...state,
        loading: true
      };
    case VOUCHER_DETAIL_FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        voucherDetails: action.payload.voucher,
        voucherImages: action.payload.voucher_photos,
        voucherOutlets: action.payload.voucher_outlets,
        voucherReviews: action.payload.voucher_reviews,
        voucherRelateds: action.payload.voucher_relateds,
        voucherSimilars: action.payload.voucher_similars
      };
    default:
      return state;
  }
};
