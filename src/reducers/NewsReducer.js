import { 
  NEWS_FETCH_SUCCESS, 
  NEWS_FETCH_LOADING,
  NEWS_SELECTED,
  NEWS_FILTERED
} from '../resources/values/actionTypes';

const INITIAL_STATE = {
  loading: false,
  newsList: {
    semua: [],
    berita: [],
    kuliner: [],
    acara: [],
    infografis: [],
    komik: []
  },
  selectedNews: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NEWS_FETCH_SUCCESS:
      return { 
        ...state, 
        loading: false, 
        newsList: action.payload 
      };
    case NEWS_FETCH_LOADING:
      return { ...state, loading: true };
    case NEWS_SELECTED:
      return { ...state, selectedNews: action.payload };
    case NEWS_FILTERED:
      return { ...state, newsList: action.payload };
    default:
      return state;
  }
};
