import { APP_TAB_CHANGED } from '../resources/values/actionTypes';
import initialState from '../reducers/initialState';

export default (state = initialState.app, action) => {
  switch (action.type) {
    case APP_TAB_CHANGED:
      return { ...state, tab: action.payload };
    default:
      return state;
  }
};

