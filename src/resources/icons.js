const icons = {
  /* NAV BAR */
  mapsUnselected: require('./icons/nav-bar/maps-1-unselected.png'),
  mapsSelected: require('./icons/nav-bar/maps-1-selected.png'),
  newsUnselected: require('./icons/nav-bar/news-1-unselected.png'),
  newsSelected: require('./icons/nav-bar/news-1-selected.png'),
  voucherUnselected: require('./icons/nav-bar/voucher-1-unselected.png'),
  voucherSelected: require('./icons/nav-bar/voucher-1-selected.png'),
  partnerUnselected: require('./icons/nav-bar/partner-1-unselected.png'),
  partnerSelected: require('./icons/nav-bar/partner-1-selected.png'),
  meUnselected: require('./icons/nav-bar/me-1-unselected.png'),
  meSelected: require('./icons/nav-bar/me-1-selected.png'),
  leftArrow: require('./icons/nav-bar/left-arrow.png'),
  /* MAPS */
  culinarySelected: require('./icons/maps/culinary-map-selected.png'),
  culinaryUnselected: require('./icons/maps/culinary-map-unselected.png'),
  filterCulinarySelected: require('./icons/menus/culinary-menu-selected.png'),
  filterCulinaryUnselected: require('./icons/menus/culinary-menu-unselected.png'),
  hotelSelected: require('./icons/maps/hotel-map-selected.png'),
  hotelUnselected: require('./icons/maps/hotel-map-unselected.png'),
  filterHotelSelected: require('./icons/menus/hotel-menu-selected.png'),
  filterHotelUnselected: require('./icons/menus/hotel-menu-unselected.png'),
  otherSelected: require('./icons/maps/other-map-selected.png'),
  otherUnselected: require('./icons/maps/other-map-unselected.png'),
  filterOtherSelected: require('./icons/menus/others-menu-selected.png'),
  filterOtherUnselected: require('./icons/menus/others-menu-unselected.png'),
  tourSelected: require('./icons/maps/tour-map-selected.png'),
  tourUnselected: require('./icons/maps/tour-map-unselected.png'),
  filterTourSelected: require('./icons/menus/tour-menu-selected.png'),
  filterTourUnselected: require('./icons/menus/tour-menu-unselected.png'),
  transportationSelected: require('./icons/maps/transportation-map-selected.png'),
  transportationUnselected: require('./icons/maps/transportation-map-unselected.png'),
  filterTransportationSelected: require('./icons/menus/transportation-menu-selected.png'),
  filterTransporationUnselected: require('./icons/menus/transportation-menu-unselected.png'),
  /* ADDITIONAL ICONS */
  clock: require('./icons/clock.png'),
  location: require('./icons/location.png'),
  myLocation: require('./icons/maps/my-location-1.png'),
  star: require('./icons/star.png'),
  greyStar: require('./icons/greyStar.png'),
  filter: require('./icons/filter.png')
};

export default icons;
