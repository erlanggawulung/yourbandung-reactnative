export default {
  navigatorStyle: {
    navBarTextColor: '#000000',
    navBarBackgroundColor: '#8fd855',
    navBarButtonColor: '#5ca622',
    navBarTitleTextCentered: true,
    statusBarTextColorScheme: 'light',
    statusBarColor: '#1b5e20'
  },
  navigator2Style: {
    navBarTextColor: '#000000',
    navBarBackgroundColor: '#8fd855',
    navBarButtonColor: '#5ca622',
    navBarTitleTextCentered: true,
    statusBarTextColorScheme: 'light',
    statusBarColor: '#1b5e20',
    tabBarHidden: true
  },
  navigatorHomeStyle: {
    navBarTextColor: '#000000',
    navBarBackgroundColor: '#8fd855',
    navBarButtonColor: '#5ca622',
    navBarTitleTextCentered: true,
    statusBarTextColorScheme: 'light',
    statusBarColor: '#1b5e20',
    navBarHidden: true,
    tabBarHidden: true
  },
  clean: {
    statusBarColor: '#1b5e20',
    statusBarTextColorScheme: 'light',
    navBarHidden: true,
    tabBarHidden: true,
    statusBarBlur: true
  }
};
