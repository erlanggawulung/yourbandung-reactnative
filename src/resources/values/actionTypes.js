/* APPLICATION */
export const APP_TAB_CHANGED = 'app_tab_changed';

/* NEWS */
export const NEWS_FETCH_LOADING = 'news_fetch_loading';
export const NEWS_FETCH_SUCCESS = 'news_fetch_success';
export const NEWS_SELECTED = 'news_selected';
export const NEWS_FILTERED = 'news_filtered';

/* MAPS */
export const MAPS_FETCH_MARKERS_SUCCESS = 'maps_fetch_markers_success';
export const MAPS_SET_CURRENT_LOCATION = 'maps_set_current_location';
export const MAPS_SELECT_MARKER = 'maps_select_marker';
export const MAPS_UNSELECT_MARKER = 'maps_unselect_marker';
export const MAPS_OPEN_CLOSE_FILTER = 'maps_open_close_filter';
export const MAPS_SET_FILTER = 'maps_set_filter';

/* PARTNER */
export const PARTNER_SELECT = 'partner_select';
export const PARTNER_DETAIL = 'partner_detail';
export const PARTNER_DETAIL_FETCH_SUCCESS = 'partner_detail_fetch_success';
export const PARTNERS_FETCH_SUCCESS = 'partners_fetch_success';

/* VOUCHER */
export const VOUCHERS_FETCH_SUCCESS = 'vouchers_fetch_success';
export const VOUCHER_SET_STAR_FILTER = 'voucher_set_star_filter';
export const VOUCHER_SET_PRICE = 'voucher_set_price';
export const VOUCHER_FILTER = 'voucher_filter';
export const VOUCHER_RESET_FILTER = 'voucher_reset_filter';
export const VOUCHER_SELECT = 'voucher_select';
export const VOUCHER_DETAILS = 'voucher_details';
export const VOUCHER_DETAIL_FETCH_SUCCESS = 'voucher_detail_fetch_success';

/* AUTH */
export const NAME_CHANGED = 'name_changed';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const PASSWORD_CONFIRMATION_CHANGED = 'password_confirmation_changed';
export const GENDER_CHANGED = 'gender_changed';
export const LOGIN_USER = 'login_user';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGOUT_USER = 'logout_user';
export const SIGNUP_USER = 'signup_user';
export const SIGNUP_USER_SUCCESS = 'signup_user_success';
export const SIGNUP_USER_FAIL = 'signup_user_fail';
export const RESET = 'reset';

/* ME */
export const FETCH_PROFILE = 'fetch_profile';
export const FETCH_PROFILE_SUCCESS = 'fetch_profile_success';
export const FETCH_PROFILE_FAIL = 'fetch_profile_fail';
