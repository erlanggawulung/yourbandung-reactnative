const images = {
  mapsUnselected: require('./images/maps-1-unselected.png'),
  mapsSelected: require('./images/maps-1-selected.png'),
  newsUnselected: require('./images/news-1-unselected.png'),
  newsSelected: require('./images/news-1-selected.png'),
  voucherUnselected: require('./images/voucher-1-unselected.png'),
  voucherSelected: require('./images/voucher-1-selected.png'),
  partnerUnselected: require('./images/partner-1-unselected.png'),
  partnerSelected: require('./images/partner-1-selected.png'),
  meUnselected: require('./images/me-1-unselected.png'),
  meSelected: require('./images/me-1-selected.png'),
  leftArrow: require('./images/left-arrow.png'),
  restaurant: require('./images/restaurant.jpg'),
  dealsLogo: require('./images/deals-logo.png'),
  yourbandungLogo: require('./images/yourbandung-logo.png'),
  hotelSample: require('./images/hotel-sample.png'),
  foodSample: require('./images/food-sample.jpg')
};

export default images;
