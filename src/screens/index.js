import { Navigation } from 'react-native-navigation';

import Home from '../modules/home/Home';
import Maps from '../modules/maps/Maps';
import MapsPartnerDetails from '../modules/maps/MapsPartnerDetails';
import News from '../modules/news/News';
import NewsContent from '../modules/news/NewsContent';
import Voucher from '../modules/voucher/Voucher';
import VoucherFilter from '../modules/voucher/VoucherFilter';
import VoucherDetails from '../modules/voucher/VoucherDetails';
import VoucherConfirm from '../modules/voucher/VoucherConfirm';
import Partner from '../modules/partner/Partner';
import PartnerDetails from '../modules/partner/PartnerDetails';
import Me from '../modules/me/Me';
import Login from '../modules/me/LoginForm';
import Signup from '../modules/me/SignupForm';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('yourbandung.Home', () => Home, store, Provider);
  Navigation.registerComponent('yourbandung.Maps', () => Maps, store, Provider);
  Navigation.registerComponent('yourbandung.MapsPartnerDetails', () => 
    MapsPartnerDetails, store, Provider);  
  Navigation.registerComponent('yourbandung.News', () => News, store, Provider);
  Navigation.registerComponent('yourbandung.NewsContent', () => NewsContent, store, Provider);
  Navigation.registerComponent('yourbandung.Voucher', () => Voucher, store, Provider);
  Navigation.registerComponent('yourbandung.VoucherFilter', () => VoucherFilter, store, Provider);
  Navigation.registerComponent('yourbandung.VoucherDetails', () => VoucherDetails, store, Provider);
  Navigation.registerComponent('yourbandung.VoucherConfirm', () => VoucherConfirm, store, Provider);
  Navigation.registerComponent('yourbandung.Partner', () => Partner, store, Provider);
  Navigation.registerComponent('yourbandung.Me', () => Me, store, Provider);
  Navigation.registerComponent('yourbandung.Login', () => Login, store, Provider);
  Navigation.registerComponent('yourbandung.Signup', () => Signup, store, Provider);
  Navigation.registerComponent('yourbandung.PartnerDetails', () => PartnerDetails, store, Provider);
}

